# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.4.26 - 2023-06-26(09:39:46 +0000)

### Other

- Rename LIBDIR to STAGING_LIBDIR

## Release v0.4.25 - 2023-06-26(07:52:15 +0000)

## Release v0.4.24 - 2023-01-27(10:28:04 +0000)

## Release v0.4.23 - 2023-01-27(08:31:42 +0000)

### Fixes

- 2 simultaneous InstallDu commands could cause a crash

### Other

- Fix possible segfault
- rlyeh should return an error the auth failed during Rlyeh.pull()
- installDU returns informative data when failed
- Add define for RemainingDiskSpaceBytes
- Rlyeh could crash when an invalid index file was read

## Release v0.4.22 - 2022-10-21(15:19:23 +0000)

### Other

- installDU returns informative data when failed

## Release v0.4.21 - 2022-10-07(12:58:54 +0000)

### Fixes

- d9e040ac shorten_image_name return value should be freed

### Other

- Remove warnings from tests

## Release v0.4.20 - 2022-10-05(20:11:46 +0000)

### Fixes

- 2 simultaneous InstallDu commands could cause a crash

## Release v0.4.19 - 2022-09-02(12:27:25 +0000)

### Other

- rlyeh should return an error the auth failed during Rlyeh.pull()

## Release v0.4.18 - 2022-06-30(16:29:59 +0000)

### Other

- Fix possible segfault

## Release v0.4.17 - 2022-06-07(09:06:34 +0000)

### Other

- Signature location shall follow image path & version

## Release v0.4.16 - 2022-04-06(13:21:46 +0000)

### Other

- : Change bundle name algorithm due to BundleName too long

## Release v0.4.15 - 2022-04-04(08:34:45 +0000)

## Release v0.4.14 - 2022-03-23(10:44:09 +0000)

### Other

- Error with notifications when installing 2 applications at the same time

## Release v0.4.13 - 2022-03-03(09:23:52 +0000)

### Other

- Incorrect Error handling in case of installing an already present DU

## Release v0.4.12 - 2022-03-01(14:56:25 +0000)

### Other

- Rlyeh crash when trying to uninstall one of the two installed DUs

## Release v0.4.11 - 2022-01-10(18:10:36 +0000)

## Release v0.4.10 - 2022-01-07(13:55:53 +0000)

## Release v0.4.9 - 2021-12-23(10:37:49 +0000)

### Fixes

- fix NULL pointer access

## Release v0.4.8 - 2021-12-15(14:30:49 +0000)

## Release v0.4.7 - 2021-11-30(11:24:31 +0000)

### Other

- [Rlyeh] Add support for docker.io

## Release v0.4.6 - 2021-11-24(10:14:19 +0000)

### Other

- [Rlyeh] Add support for docker.io

## Release v0.4.5 - 2021-11-05(13:06:03 +0000)

### Other

- libRlyeh : keep track of shared layers

## Release v0.4.4 - 2021-11-04(15:32:55 +0000)

### Other

- [Rlyeh] Implement 'Status' parameter

## Release v0.4.3 - 2021-10-27(12:58:03 +0000)

### Other

- [Rlyeh] Verify the OCI image signature

## Release v0.4.2 - 2021-09-27(09:03:17 +0000)

### Fixes

- Fix rename for old compilers

## Release v0.4.1 - 2021-09-24(09:32:54 +0000)

### Fixes

- [librlyeh] rename on cross device links not working

## Release v0.4.0 - 2021-09-23(11:58:30 +0000)

### New

- Issue [IOT-886] : [Rlyeh] Verify checksum after pulling image's blobs

## Release v0.3.3 - 2021-09-13(09:22:13 +0000)

### Fixes

- rework rlyeh_remove

## Release v0.3.2 - 2021-09-06(10:31:54 +0000)

### Fixes

- add a config for external

## Release v0.3.1 - 2021-09-06(10:04:00 +0000)

### Other

- Add changenlog
- Update .gitlab-ci.yml
- [lib rlyeh] Add remove to ckopeo

## Release v0.3.0 - 2021-09-03(09:48:30 +0000)

### New

- Initial release
