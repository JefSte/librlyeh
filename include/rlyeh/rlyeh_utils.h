/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__RLYEH_UTILS_H__)
#define __RLYEH_UTILS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <rlyeh/rlyeh_auth.h>
#include <amxc/amxc.h>
#include <amxc/amxc_string.h>

#include <libocispec/image_spec_schema_image_manifest_schema.h>
#include <libocispec/image_spec_schema_image_index_schema.h>
#include <stdbool.h>

#define RLYEH_ERR_MSG_LEN 1024

typedef struct _rlyeh_image_parameters {
    char* transport;
    char* image_name;
    char* version;
    amxc_string_t server;
    rlyeh_auth_type_t auth_type;
    amxc_string_t username;
    amxc_string_t password;
    amxc_string_t token;
    bool sv; /* Signature Verification enable */
} rlyeh_image_parameters_t;

int rlyeh_image_parameters_init(rlyeh_image_parameters_t* data);
int rlyeh_image_parameters_clean(rlyeh_image_parameters_t* data);

bool file_exists(const char* filename);
bool dir_exists(const char* dirname);
bool image_already_exists(const rlyeh_image_parameters_t* image);

amxc_var_t* read_config(const char* file);

char* shorten_image_name(const char* name);

int rlyeh_parse_uri(const char* uri, rlyeh_image_parameters_t* uri_elements);
int rlyeh_parse_local_image(const char* data, rlyeh_image_parameters_t* path_parse);

image_spec_schema_image_index_schema* rlyeh_parse_image_spec_schema(const char* image_name);

int rlyeh_get_creds_from_cmd(char* data, rlyeh_image_parameters_t* path_parse);

int rlyeh_get_creds_from_file(rlyeh_image_parameters_t* param);

int rlyeh_get_server_from_file(rlyeh_image_parameters_t* param);

bool build_manifest_filename_from_index(const image_spec_schema_image_index_schema* index_schema, const char* image_version, const char* storage_location, amxc_string_t* manifest_filename);
bool check_manifest_validity(const image_spec_schema_image_manifest_schema* manifest, parser_error err);

int rlyeh_hash_blob(const char* filename, char outputBuffer[65]);

int complete_uri(const char* uri, amxc_string_t* full_uri);

int build_fname_from_digest(const char* storage_location, const char* digest, amxc_string_t* fname);

int set_err_msg(char* dest, const char* msg, ...);

#ifdef __cplusplus
}
#endif

#endif // __RLYEH_UTILS_H__
