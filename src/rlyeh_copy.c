/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <sys/statvfs.h>

#include <rlyeh/rlyeh.h>
#include <rlyeh_copy_priv.h>
#include <rlyeh_priv.h>
#include <rlyeh_assert.h>
#include <libocispec/image_spec_schema_image_manifest_schema.h>
#include <debug/sahtrace.h>

#define ME "rlyeh_copy"

rlyeh_status_t size_check(const int64_t expected_size, const long downloaded_size) {
    // SAH_TRACEZ_INFO(ME, "Check size [%ld][%ld]", expected_size, downloaded_size);
    if(!(expected_size == downloaded_size)) {
        return RLYEH_ERROR_SIZE_CHECK;
    } else {
        return RLYEH_NO_ERROR;
    }
}

// Returns true if there is an error.
static bool create_directory(const char* dirname, int mode) {
    struct stat s;
    int check;
    if((stat(dirname, &s) != 0) || !S_ISDIR(s.st_mode)) {
        check = mkdir(dirname, mode);
        if(check) {
            SAH_TRACEZ_ERROR(ME, "ERROR: Cannot create %s directory...", dirname);
            return true;
        }
    }
    return false;
}

// Recursive directory creation
static bool create_directories(const char* dirname, int mode) {
    char* path;
    char* subpath;
    char* copypath = strdup(dirname);
    bool status = false;

    path = copypath;
    while(!status && (subpath = strchr(path, '/')) != 0) {
        if(subpath != path) {
            /* Neither root nor double slash in path */
            *subpath = '\0';
            // fprintf(stderr, "trying to  create %s\n", copypath);
            status = create_directory(copypath, mode);
            *subpath = '/';
        }
        path = subpath + 1;
    }
    if(status == false) {
        // fprintf(stderr, "trying to  create %s\n", dirname);
        status = create_directory(dirname, mode);
    }
    free(copypath);
    return status;
}

rlyeh_status_t set_tmp_blob_filename(const char* digest,
                                     const rlyeh_copy_output_directories_t* output_dir,
                                     amxc_string_t* filename,
                                     char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    char* sha_dir = NULL;
    char* pos = NULL;
    char* blob_file = NULL;
    char* saveptr = NULL;
    amxc_string_t blob_dir;
    amxc_string_init(&blob_dir, 0);

    // Modify sha256:ad23e3c2fa54c2e1... TO sha256/ad23e3c2fa54c2e1...
    blob_file = (char*) malloc(strlen(digest) + 1);
    strcpy(blob_file, digest);
    if((pos = strchr(blob_file, ':')) != NULL) {
        *pos = '/';
    }

    amxc_string_setf(filename, "%s/%s.tmp", output_dir->blobs.buffer, blob_file);

    // Extract the SHA prefix to create a specific directory
    sha_dir = strtok_r(blob_file, "/", &saveptr);

    amxc_string_setf(&blob_dir, "%s/%s", output_dir->blobs.buffer, sha_dir);

    if(create_directories(blob_dir.buffer, 0755)) {
        set_err_msg(err_msg, "Failed to create %s", blob_dir.buffer);
        ret = RLYEH_ERROR_DISK_SPACE;
    }

    amxc_string_clean(&blob_dir);
    free(blob_file);

    return ret;
}

static bool verify_checksum(const amxc_string_t* filename, const amxc_string_t* tmp_filename) {
    bool ret = false;
    char blob_hash[65];
    char* cpy_filename = strdup(filename->buffer);
    char* cpy_tmp_filename = strdup(tmp_filename->buffer);

    char* hash_name = strrchr(cpy_filename, '/');

    rlyeh_hash_blob(cpy_tmp_filename, blob_hash);

    if(strcmp(hash_name + 1, blob_hash)) {
        SAH_TRACEZ_ERROR(ME, "Incorrect hash for file [%s]", filename->buffer);
        ret = true;
    }

    free(cpy_filename);
    free(cpy_tmp_filename);
    return ret;
}

static rlyeh_status_t move_tmp_blob(const amxc_string_t* tmp_filename, const amxc_string_t* filename) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;

    if(verify_checksum(filename, tmp_filename)) {
        remove(tmp_filename->buffer);
        ret = RLYEH_ERROR_INVALID_CONTENT;
    } else {
        rename(tmp_filename->buffer, filename->buffer);
    }

    return ret;
}

static rlyeh_status_t retrieve_manifest(const rlyeh_image_parameters_t* parameters,
                                        const rlyeh_copy_output_directories_t* output_dir,
                                        amxc_string_t* file_retrieved,
                                        curl_off_t* size,
                                        char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    amxc_string_t url;
    rlyeh_curl_parameters_t manifest_parameters = {
        .username = &parameters->username,
        .password = &parameters->password,
        .token = &parameters->token,
        .type = FILE_TYPE_MANIFEST,
        .auth_type = parameters->auth_type
    };
    amxc_string_init(&url, 0);
    amxc_string_setf(&url, "https://%s%s/manifests/%s", parameters->server.buffer, parameters->image_name, parameters->version);

    amxc_string_setf(file_retrieved, "%s/manifest.json", output_dir->image.buffer);

    if(file_exists(file_retrieved->buffer)) {
        SAH_TRACEZ_WARNING(ME, "File %s already exists", file_retrieved->buffer);
    }

    FILE* fd = fopen("/dev/null", "w");
    ret = rlyeh_curl_head(&manifest_parameters, &url, (void*) fd, size, err_msg);
    fclose(fd);
    when_failed(ret, exit);

    // TODO : check disk space before download content
    // struct statvfs st;
    // statvfs(file_retrieved->buffer, &st);
    // unsigned long free_space = st.f_bfree * st.f_frsize;
    // fprintf(stderr, "manifest size : %ld\n", *size);
    // fprintf(stderr, "free space : %ld\n", free_space);
    // fprintf(stderr, "f_frsize : %ld\n", st.f_frsize);

    fd = fopen(file_retrieved->buffer, "w");
    ret = rlyeh_curl_download_content(&manifest_parameters, &url, (void*) fd, size, err_msg);
    fclose(fd);
    if(ret) {
        remove(file_retrieved->buffer);
    }

exit:
    amxc_string_clean(&url);
    return ret;
}

static rlyeh_status_t retrieve_config(const rlyeh_image_parameters_t* parameters,
                                      const char* digest,
                                      const rlyeh_copy_output_directories_t* output_dir,
                                      curl_off_t* size,
                                      char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    amxc_string_t tmp_filename;
    amxc_string_t filename;
    amxc_string_t url;
    rlyeh_curl_parameters_t config_parameters = {
        .username = &parameters->username,
        .password = &parameters->password,
        .token = &parameters->token,
        .type = FILE_TYPE_CONFIG,
        .auth_type = parameters->auth_type
    };
    amxc_string_init(&tmp_filename, 0);
    amxc_string_init(&filename, 0);

    amxc_string_init(&url, 0);
    amxc_string_setf(&url, "https://%s%s/blobs/%s", parameters->server.buffer, parameters->image_name, digest);

    ret = set_tmp_blob_filename(digest, output_dir, &tmp_filename, err_msg);
    when_failed(ret, exit);

    // Shrink to check if the blob already exists
    amxc_string_copy(&filename, &tmp_filename);
    amxc_string_shrink(&filename, 4);

    if(!file_exists(filename.buffer)) {
        FILE* fd = fopen(tmp_filename.buffer, "w");
        ret = rlyeh_curl_download_content(&config_parameters, &url, (void*) fd, size, err_msg);
        fclose(fd);
        if(ret) {
            remove(tmp_filename.buffer);
            goto exit;
        }
        ret = move_tmp_blob(&tmp_filename, &filename);
    } else {
        SAH_TRACEZ_INFO(ME, "File %s already exists", filename.buffer);
    }

exit:
    amxc_string_clean(&tmp_filename);
    amxc_string_clean(&filename);
    amxc_string_clean(&url);
    return ret;
}

static rlyeh_status_t retrieve_layer(const rlyeh_image_parameters_t* parameters,
                                     const char* digest,
                                     const rlyeh_copy_output_directories_t* output_dir,
                                     curl_off_t* size,
                                     char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    amxc_string_t tmp_filename;
    amxc_string_t filename;
    amxc_string_t url;
    rlyeh_curl_parameters_t layer_parameters = {
        .username = &parameters->username,
        .password = &parameters->password,
        .token = &parameters->token,
        .type = FILE_TYPE_BLOB,
        .auth_type = parameters->auth_type
    };

    amxc_string_init(&tmp_filename, 0);
    amxc_string_init(&filename, 0);

    amxc_string_init(&url, 0);
    amxc_string_setf(&url, "https://%s%s/blobs/%s", parameters->server.buffer, parameters->image_name, digest);

    ret = set_tmp_blob_filename(digest, output_dir, &tmp_filename, err_msg);
    when_failed(ret, exit);

    // Shrink to check if the blob already exists
    amxc_string_copy(&filename, &tmp_filename);
    amxc_string_shrink(&filename, 4);

    if(!file_exists(filename.buffer)) {
        FILE* fd = fopen(tmp_filename.buffer, "w");
        ret = rlyeh_curl_download_content(&layer_parameters, &url, (void*) fd, size, err_msg);
        fclose(fd);
        if(ret) {
            remove(tmp_filename.buffer);
            goto exit;
        }
        ret = move_tmp_blob(&tmp_filename, &filename);
    } else {
        SAH_TRACEZ_INFO(ME, "File %s already exists", filename.buffer);
    }

exit:
    amxc_string_clean(&tmp_filename);
    amxc_string_clean(&filename);
    amxc_string_clean(&url);

    return ret;
}

static void generate_index(rlyeh_index_generation_data_t* index_data) {
    char* index_content;
    amxc_string_t index_filename;
    amxc_string_init(&index_filename, 0);

    amxc_string_setf(&index_filename, "%s/index.json", index_data->image_name);

    index_content = rlyeh_image_index_generate_content(index_data);
    when_null(index_content, exit);

    rlyeh_write_index(index_filename.buffer, index_content);
    SAH_TRACEZ_INFO(ME, "Creating index.json");

exit:
    free(index_content);
    amxc_string_clean(&index_filename);
}

static rlyeh_status_t download_config(const rlyeh_image_parameters_t* parameters,
                                      const image_spec_schema_image_manifest_schema* image_manifest,
                                      const rlyeh_copy_output_directories_t* output_dir,
                                      char* err_msg) {
    curl_off_t size = -1;
    rlyeh_status_t ret = retrieve_config(parameters, image_manifest->config->digest, output_dir, &size, err_msg);
    when_failed(ret, exit);
    if(image_manifest->config->size_present && (size != -1)) {
        ret = size_check(image_manifest->config->size, size);
        if(ret) {
            set_err_msg(err_msg, "Download config [%s] size mismatch (expected: %d downloaded %d)",
                        image_manifest->config->digest, image_manifest->config->size, size);
            goto exit;
        }
    }
exit:
    return ret;
}

static rlyeh_status_t download_layers(const rlyeh_image_parameters_t* parameters,
                                      const image_spec_schema_image_manifest_schema* image_manifest,
                                      const rlyeh_copy_output_directories_t* output_dir,
                                      char* err_msg) {
    curl_off_t size = -1;
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    if(image_manifest->layers_len != 0) {
        int i;
        for(i = 0; i < (int) image_manifest->layers_len; i++) {
            ret = retrieve_layer(parameters, image_manifest->layers[i]->digest, output_dir, &size, err_msg);
            if(ret) {
                SAH_TRACEZ_ERROR(ME, "Error pulling layer %s", image_manifest->layers[i]->digest);
                goto exit;
            }
            if(image_manifest->layers[i]->size_present && (size != -1)) {
                ret = size_check(image_manifest->layers[i]->size, size);
                if(ret) {
                    set_err_msg(err_msg, "Download layer [%s] size mismatch (expected: %d downloaded %d)",
                                image_manifest->layers[i]->digest, image_manifest->layers[i]->size, size);
                    goto exit;
                }
            }
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "No layer in the manifest");
    }
exit:
    return ret;

}

bool send_file_manifest(const amxc_string_t* manifest_filename, const amxc_string_t* new_name) {
    // Try sendfile, there probably is a file system boundary
    struct stat st;
    off_t off = 0;
    int rv;
    int fromfd, tofd;
    char* tofile = new_name->buffer;
    char* fromfile = manifest_filename->buffer;

    if(stat(fromfile, &st) < 0) {
        SAH_TRACEZ_ERROR(ME, "Can't stat '%s': (%d) %s", fromfile, errno, strerror(errno));
        return true;
    }

    if(((unlink(tofile)) < 0) && (errno != ENOENT)) {
        SAH_TRACEZ_ERROR(ME, "unlink");
        return true;
    }

    errno = 0;
    if(((fromfd = open(fromfile, O_RDONLY)) < 0) ||
       ((tofd = open(tofile, O_WRONLY | O_CREAT, 0644)) < 0)) {
        SAH_TRACEZ_ERROR(ME, "Can't open '%s' or '%s' - (%d) %s", fromfile, tofile, errno, strerror(errno));
        return true;
    }

    if((rv = sendfile(tofd, fromfd, &off, (size_t) st.st_size)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Warning: sendfile(3EXT) returned %d (errno (%d) %s)\n", rv, errno, strerror(errno));
        return true;
    }

    SAH_TRACEZ_INFO(ME, "Sendfile succesful");
    return false;

}

static rlyeh_status_t rename_manifest(const amxc_string_t* manifest_filename, const rlyeh_copy_output_directories_t* output_dir, rlyeh_index_generation_data_t* const index_data) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    amxc_string_t blob_dir;
    amxc_string_t new_name;
    amxc_string_init(&blob_dir, 0);
    amxc_string_init(&new_name, 0);

    // Create blob directory
    amxc_string_setf(&blob_dir, "%s/sha256", output_dir->blobs.buffer);
    create_directories(blob_dir.buffer, 0755);

    // Rename manifest.json with the digest contained in the index
    amxc_string_setf(&new_name, "%s/sha256/%s", output_dir->blobs.buffer, index_data->manifest_digest);
    if(rename(manifest_filename->buffer, new_name.buffer)) {
        SAH_TRACEZ_WARNING(ME, "Unable to rename the file: (%d) %s", errno, strerror(errno));
        if(errno == EXDEV) {
            if(send_file_manifest(manifest_filename, &new_name)) {
                ret = RLYEH_ERROR_UNKNOWN_ERROR;
            }
        }
    } else {
        SAH_TRACEZ_INFO(ME, "File renamed successfully");
    }

    amxc_string_clean(&new_name);
    amxc_string_clean(&blob_dir);

    return ret;
}

// This function download the manifest and the blobs (config and layers)
static rlyeh_status_t download_image_files(rlyeh_image_parameters_t* parameters,
                                           rlyeh_index_generation_data_t* const index_data,
                                           const rlyeh_copy_output_directories_t* output_dir,
                                           char* err_msg) {
    image_spec_schema_image_manifest_schema* image_manifest = NULL;
    parser_error err = NULL;
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    curl_off_t size = 0;
    amxc_string_t manifest_filename;
    rlyeh_signature_data_t sigdata;
    amxc_string_init(&manifest_filename, 0);
    rlyeh_signature_data_init(&sigdata);
    sigdata.image_name = strdup(parameters->image_name);
    sigdata.transport = strdup(parameters->transport);
    sigdata.server = strdup(parameters->server.buffer);
    sigdata.version = strdup(parameters->version);

    // Normalize server name
    // "/v2/" is appened to the server to fit with API V2 container image registries
    amxc_string_append(&parameters->server, "/v2/", strlen("/v2/"));

    // Retrieve manifest.json
    ret = retrieve_manifest(parameters, output_dir, &manifest_filename, &size, err_msg);
    when_failed(ret, exit);

    image_manifest
        = image_spec_schema_image_manifest_schema_parse_file(manifest_filename.buffer,
                                                             NULL,
                                                             &err);

    if(check_manifest_validity(image_manifest, err)) {
        set_err_msg(err_msg, "Image manifest is invalid err[%s] [%s]", err, manifest_filename.buffer);
        ret = RLYEH_ERROR_INVALID_CONTENT;
        remove(manifest_filename.buffer);
        goto exit;
    }

    // Hash manifest
    index_data->manifest_size = rlyeh_hash_blob(manifest_filename.buffer, index_data->manifest_digest);

    // Verify image signature
    if(parameters->sv) {
        if(rlyeh_read_policy("/etc/amx/librlyeh/policy.json", sigdata.transport, sigdata.server) == GPG_KEY_SIGNATURE) {
            SAH_TRACEZ_INFO(ME, "Signature Verification");
            sigdata.manifest_hash = strdup(index_data->manifest_digest);
            if(rlyeh_verify_signature(&sigdata)) {
                set_err_msg(err_msg, "Image signarure is invalid [%s]", manifest_filename.buffer);
                remove(manifest_filename.buffer);
                ret = RLYEH_ERROR_INVALID_SIGNATURE;
                goto exit;
            }
            SAH_TRACEZ_INFO(ME, "Valid signature");
        }
    }

    ret = rename_manifest(&manifest_filename, output_dir, index_data);
    if(ret != RLYEH_NO_ERROR) {
        set_err_msg(err_msg, "Manifest renaming failed [%s]", manifest_filename.buffer);
        goto exit;
    }

    ret = download_config(parameters, image_manifest, output_dir, err_msg);
    when_failed(ret, exit);

    ret = download_layers(parameters, image_manifest, output_dir, err_msg);
    when_failed(ret, exit);

exit:
    free(err);
    rlyeh_signature_data_clean(&sigdata);
    amxc_string_clean(&manifest_filename);
    if(image_manifest) {
        free_image_spec_schema_image_manifest_schema(image_manifest);
        image_manifest = NULL;
    }
    return ret;
}

static rlyeh_status_t create_output_directories(const rlyeh_copy_data_t* data,
                                                const rlyeh_image_parameters_t* image_parameters,
                                                rlyeh_copy_output_directories_t* output_dir,
                                                char* err_msg) {
    // Create image directory and default blob directory
    amxc_string_set(&output_dir->image, image_parameters->image_name);

    if(create_directories(output_dir->image.buffer, 0755)) {
        set_err_msg(err_msg, "Could not create dir %s", output_dir->image.buffer);
        return RLYEH_ERROR_DISK_SPACE;
    }

    // Create blobs directory
    if(data->dest_shared_blob_dir) {
        amxc_string_reset(&output_dir->blobs);
        amxc_string_set(&output_dir->blobs, data->dest_shared_blob_dir);
    } else {
        amxc_string_copy(&output_dir->blobs, &output_dir->image);
        amxc_string_append(&output_dir->blobs, "/blobs", strlen("/blobs"));
    }

    if(create_directories(output_dir->blobs.buffer, 0755)) {
        set_err_msg(err_msg, "Could not create dir %s", output_dir->blobs.buffer);
        return RLYEH_ERROR_DISK_SPACE;
    }

    return RLYEH_NO_ERROR;
}

static rlyeh_status_t print_parse_input(const rlyeh_copy_data_t* input_data, rlyeh_image_parameters_t* src, rlyeh_image_parameters_t* dest) {
    SAH_TRACEZ_INFO(ME, "SRC: %s DEST: %s", input_data->source, input_data->destination);

    if(input_data->dest_shared_blob_dir) {
        SAH_TRACEZ_INFO(ME, "DEST-SHARED-BLOB-DIR: %s", input_data->dest_shared_blob_dir);
    }

    if(input_data->user) {
        SAH_TRACEZ_INFO(ME, "USER: %s", input_data->user);
    }

    // Parse arguments
    rlyeh_parse_uri(input_data->source, src);
    rlyeh_parse_local_image(input_data->destination, dest);
    src->sv = input_data->sv;

    // Dest must be OCI
    if(strcmp(dest->transport, "oci")) {
        SAH_TRACEZ_ERROR(ME, "dest MUST be 'oci' [%s]", dest->transport);
        return RLYEH_ERROR_INVALID_ARGUMENTS;
    }

    // Set default server if not in uri
    if(amxc_string_is_empty(&src->server)) {
        rlyeh_get_server_from_file(src);
    }

    return RLYEH_NO_ERROR;
}
static rlyeh_status_t download_token(const amxc_string_t* url, rlyeh_image_parameters_t* data, char* err_msg) {
#define TOKEN_FNAME "/tmp/token"
    rlyeh_status_t status = RLYEH_NO_ERROR;
    amxc_var_t* config_data = NULL;
    FILE* token = fopen(TOKEN_FNAME, "w");
    status = rlyeh_curl_get(url, token, err_msg);
    fclose(token);
    when_failed(status, exit);

    config_data = read_config(TOKEN_FNAME);
    if(config_data) {
        amxc_var_t* var_token = GET_ARG(config_data, "token");
        const char* str_token = amxc_var_constcast(cstring_t, var_token);
        amxc_string_set(&data->token, str_token);
        amxc_var_delete(&var_token);
    }
    amxc_var_delete(&config_data);
    // fprintf(stderr, "TOKEN\n%s\n", data->token.buffer);

exit:
    remove(TOKEN_FNAME);
    return status;
}

void get_authenticate_element(const char* buf, char** realm, char** service, char** scope) {
    char* bufcpy = strdup(buf);
    char* saveptr_line = NULL;
    char* saveptr_word = NULL;
    char* line = strtok_r(bufcpy, "\r\n", &saveptr_line);
    do {
        if(strncasecmp(line, "www-authenticate", strlen("www-authenticate")) == 0) {
            // fprintf(stderr, "line(%s)\n", line);
            char* word = strtok_r(line, ", ", &saveptr_word);
            do {
                // fprintf(stderr, "word(%s)\n", word);
                if(strncasecmp(word, "realm", strlen("realm")) == 0) {
                    if(!(*realm)) {
                        *realm = (char*) calloc(1, strlen(word) - 7);
                        strncpy(*realm, word + 7, strlen(word) - 8);
                        // fprintf(stderr, "[%s]\n", *realm);
                    }
                }
                if(strncasecmp(word, "service", strlen("service")) == 0) {
                    if(!(*service)) {
                        *service = (char*) calloc(1, strlen(word) - 9);
                        strncpy(*service, word + 9, strlen(word) - 10);
                        // fprintf(stderr, "[%s]\n", *service);
                    }
                }
                if(strncasecmp(word, "scope", strlen("scope")) == 0) {
                    if(!(*scope)) {
                        *scope = (char*) calloc(1, strlen(word) - 7);
                        strncpy(*scope, word + 7, strlen(word) - 8);
                        // fprintf(stderr, "[%s]\n", *scope);
                    }
                }
            } while((word = strtok_r(NULL, ", ", &saveptr_word)));
            break;
        }
    } while((line = strtok_r(NULL, "\r\n", &saveptr_line)));

    free(bufcpy);
}

static rlyeh_status_t retrieve_token(const char* buf, rlyeh_image_parameters_t* src, char* err_msg) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    char* realm = NULL;
    char* service = NULL;
    char* scope = NULL;
    amxc_string_t url;
    amxc_string_init(&url, 0);

    get_authenticate_element(buf, &realm, &service, &scope);
    // fprintf(stderr, "[%s][%s][%s]\n", realm, service, scope);

    if(realm && service && scope) {
        amxc_string_setf(&url, "%s?scope=%s&service=%s", realm, scope, service);
    } else {
        SAH_TRACEZ_ERROR(ME, "Can't retrieve token for authentication");
        set_err_msg(err_msg, "Can't retrieve token for authentication");
        status = RLYEH_ERROR_UNKNOWN_ERROR;
        goto exit;
    }

    status = download_token(&url, src, err_msg);

exit:
    amxc_string_clean(&url);
    if(realm) {
        free(realm);
    }
    if(service) {
        free(service);
    }
    if(scope) {
        free(scope);
    }

    return status;
}

static rlyeh_status_t rlyeh_get_authentication_data(rlyeh_copy_data_t* data,
                                                    rlyeh_image_parameters_t* src,
                                                    char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    long auth;
    char* buf = NULL;
    amxc_string_t url;
    amxc_string_init(&url, 0);
    amxc_string_setf(&url, "https://%s/v2/%s/manifests/%s", src->server.buffer, src->image_name, src->version);

    ret = rlyeh_curl_get_httpauthentication(&url, (void*) &buf, &auth, err_msg);
    when_failed(ret, exit);
    // fprintf(stderr, "[%s]\r\n", buf);

    if(auth & CURLAUTH_BASIC) {
        src->auth_type = BASIC_AUTHENTICATION;
        if(data->user) {
            rlyeh_get_creds_from_cmd(data->user, src);
        } else {
            rlyeh_get_creds_from_file(src);
        }
    } else if(auth & CURLAUTH_BEARER) {
        src->auth_type = BEARER_AUTHENTICATION;
        ret = retrieve_token(buf, src, err_msg);
        when_failed(ret, exit);
    } else {
        src->auth_type = NO_AUTHENTICATION;
    }
exit:
    amxc_string_clean(&url);
    free(buf);

    return ret;
}

static void replace_in_buffer(char* str, const char* oldWord, const char* newWord) {
    char* pos, temp[512];
    int index = 0;
    int owlen = strlen(oldWord);

    if(!strcmp(oldWord, newWord)) {
        return;
    }

    while((pos = strstr(str, oldWord)) != NULL) {
        strcpy(temp, str);
        index = pos - str;
        str[index] = '\0';
        strcat(str, newWord);
        strcat(str, temp + index + owlen);
    }
}

static void patch_mediatype(const rlyeh_copy_output_directories_t* output_dir, const char* image_name, const char* version) {
    amxc_string_t fname_index;
    amxc_string_t fname_manifest;
    amxc_string_t fname_tmp;
    amxc_string_init(&fname_index, 0);
    amxc_string_init(&fname_manifest, 0);
    amxc_string_init(&fname_tmp, 0);

    amxc_string_setf(&fname_index, "%s/index.json", image_name);
    // fprintf(stderr, "index.json [%s]\n", fname_index.buffer);

    image_spec_schema_image_index_schema* index_schema = rlyeh_parse_image_spec_schema(fname_index.buffer);
    if(!index_schema) {

        SAH_TRACEZ_ERROR(ME, "Index file does not exist [%s]", fname_index.buffer);
        goto exit;
    }

    build_manifest_filename_from_index(index_schema, version, output_dir->blobs.buffer, &fname_manifest);
    free_image_spec_schema_image_index_schema(index_schema);

    // fprintf(stderr, "manifest [%s]\n", fname_manifest.buffer);
    if(!file_exists(fname_manifest.buffer)) {
        SAH_TRACEZ_ERROR(ME, "Manifest not found to patch mediatype %s", fname_manifest.buffer);
        goto exit;
    }

    // Store the patched manifest in <manifest_name>.tmp
    amxc_string_copy(&fname_tmp, &fname_manifest);
    amxc_string_append(&fname_tmp, ".tmp", strlen(".tmp"));

    // Replace mediatype
    FILE* old = fopen(fname_manifest.buffer, "r");
    FILE* renamed = fopen(fname_tmp.buffer, "w");
    char buffer[512];
    while((fgets(buffer, 512, old)) != NULL) {
        replace_in_buffer(buffer, "vnd.docker.distribution.manifest.v2", "vnd.oci.image.manifest.v1");
        replace_in_buffer(buffer, "vnd.docker.container.image.v1", "vnd.oci.image.config.v1");
        replace_in_buffer(buffer, "vnd.docker.image.rootfs.diff.tar.gzip", "vnd.oci.image.layer.v1.tar+gzip");
        fputs(buffer, renamed);
    }
    fclose(old);
    fclose(renamed);

    // Remove the old one and rename the new one
    remove(fname_manifest.buffer);
    rename(fname_tmp.buffer, fname_manifest.buffer);
exit:
    // Clean strings
    amxc_string_clean(&fname_index);
    amxc_string_clean(&fname_manifest);
    amxc_string_clean(&fname_tmp);
}

void rlyeh_copy_data_init(rlyeh_copy_data_t* data) {
    data->uuid = NULL;
    data->source = NULL;
    data->destination = NULL;
    data->dest_shared_blob_dir = NULL;
    data->user = NULL;
    data->sv = 0;
}

void rlyeh_copy_data_clean(rlyeh_copy_data_t* data) {
    if(!data) {
        return;
    }
    free(data->uuid);
    free(data->source);
    free(data->destination);
    free(data->dest_shared_blob_dir);
    free(data->user);
}

void rlyeh_copy_output_directories_init(rlyeh_copy_output_directories_t* data) {
    amxc_string_init(&data->image, 0);
    amxc_string_init(&data->blobs, 0);
}

void rlyeh_copy_output_directories_clean(rlyeh_copy_output_directories_t* data) {
    amxc_string_clean(&data->image);
    amxc_string_clean(&data->blobs);
}

rlyeh_status_t rlyeh_copy(rlyeh_copy_data_t* data, char* err_msg) {
    rlyeh_status_t retval = RLYEH_NO_ERROR;
    rlyeh_copy_output_directories_t output_dir;
    rlyeh_image_parameters_t src_parameters;
    rlyeh_image_parameters_t dest_parameters;
    rlyeh_index_generation_data_t index_data;
    amxc_string_t oci_layout_filename;

    // Initialize variables
    rlyeh_copy_output_directories_init(&output_dir);
    rlyeh_image_parameters_init(&src_parameters);
    rlyeh_image_parameters_init(&dest_parameters);
    amxc_string_init(&oci_layout_filename, 0);
    rlyeh_index_generation_data_init(&index_data);


    retval = print_parse_input(data, &src_parameters, &dest_parameters);
    if(retval != RLYEH_NO_ERROR) {
        set_err_msg(err_msg, "Failed to parse input src [%s] dest [%s]", data->source, data->destination);
        goto exit;
    }

    // If the image already exists, it overwrites the content (manifest and index.json)
    // if(image_already_exists(&dest_parameters)) {
    //     SAH_TRACEZ_INFO(ME, "Image %s:%s already exists", dest_parameters.image_name, dest_parameters.version);
    //     retval = RLYEH_IMAGE_ALREADY_EXISTS;
    //     goto exit;
    // }

    retval = create_output_directories(data, &dest_parameters, &output_dir, err_msg);
    when_failed(retval, exit);

    retval = rlyeh_get_authentication_data(data, &src_parameters, err_msg);
    when_failed(retval, exit);

    retval = download_image_files(&src_parameters, &index_data, &output_dir, err_msg);
    when_failed(retval, exit);

    // Index.json generation
    index_data.version = dest_parameters.version;
    index_data.image_name = dest_parameters.image_name;
    index_data.uri = data->source;
    index_data.uuid = data->uuid;
    index_data.mark_for_removal = false;
    generate_index(&index_data);

    // oci-layout generation
    amxc_string_setf(&oci_layout_filename, "%s/oci-layout", output_dir.image.buffer);
    rlyeh_image_oci_layout_create(oci_layout_filename.buffer);
    SAH_TRACEZ_INFO(ME, "Creating oci-layout");

    //Patch mediatype for cthulhu
    patch_mediatype(&output_dir, dest_parameters.image_name, dest_parameters.version);

exit:
    amxc_string_clean(&oci_layout_filename);
    rlyeh_copy_output_directories_clean(&output_dir);
    rlyeh_image_parameters_clean(&src_parameters);
    rlyeh_image_parameters_clean(&dest_parameters);
    return retval;
}
