/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif


#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <debug/sahtrace.h>

#include <rlyeh/rlyeh_utils.h>
#include <rlyeh_priv.h>
#include <rlyeh_assert.h>
#include <rlyeh/rlyeh_sync.h>
#include <rlyeh/rlyeh_image_info.h>
#include <rlyeh_defines_priv.h>

#include <libocispec/image_spec_schema_image_index_schema.h>
#include <libocispec/image_spec_schema_image_manifest_schema.h>

#define ME "rlyeh_sync"

void init_image_data(rlyeh_image_data_t* data) {
    if(!data) {
        return;
    }
    data->uri = NULL;
    data->uuid = NULL;
    rlyeh_image_info_init(&data->image_info);
}

void free_image_data(rlyeh_image_data_t* data) {
    if(!data) {
        return;
    }
    free(data->uri);
    free(data->uuid);
    rlyeh_image_info_clean(&data->image_info);

    free(data);
}

void free_rlyeh_sync_data(rlyeh_sync_data_t* data) {
    if(!data) {
        return;
    }
    if(data->len) {
        for(size_t it = 0; it < data->len; it++) {
            free_image_data(data->images[it]);
        }
        free(data->images);
    }
}

void init_rlyeh_sync_data(rlyeh_sync_data_t* data) {
    data->len = 0;
    data->images = NULL;
}

static void get_image_data_from_index(image_spec_schema_image_index_schema_manifests_element* manifest, rlyeh_image_data_t* image) {
    size_t index_annotations;
    for(index_annotations = 0; index_annotations < manifest->annotations->len; index_annotations++) {
        char* annotation_key = manifest->annotations->keys[index_annotations];
        if(strcmp(RLYEH_ANNOTATION_OCI_REFNAME, annotation_key) == 0) {
            image->image_info.version = strdup(manifest->annotations->values[index_annotations]);
        } else if(strcmp(RLYEH_ANNOTATION_SAH_URI, annotation_key) == 0) {
            image->uri = strdup(manifest->annotations->values[index_annotations]);
        } else if(strcmp(RLYEH_ANNOTATION_SAH_UUID, annotation_key) == 0) {
            image->uuid = strdup(manifest->annotations->values[index_annotations]);
        } else if(strcmp(RLYEH_ANNOTATION_SAH_MAKRFORREMOVAL, annotation_key) == 0) {
            image->markforremoval = atoi(manifest->annotations->values[index_annotations]);
        }
    }
}

static void get_image_data_from_manifest(const char* dirname, image_spec_schema_image_manifest_schema* manifest_schema, rlyeh_image_data_t* image) {
    if(manifest_schema->annotations) {
        size_t it_annotations;
        for(it_annotations = 0; it_annotations < manifest_schema->annotations->len; it_annotations++) {
            char* annotation_key = manifest_schema->annotations->keys[it_annotations];
            if(strcmp(RLYEH_ANNOTATION_OCI_DESCRIPTION, annotation_key) == 0) {
                if(image->image_info.description == NULL) {
                    image->image_info.description = strdup(manifest_schema->annotations->values[it_annotations]);
                }
            } else if(strcmp(RLYEH_ANNOTATION_OCI_VENDOR, annotation_key) == 0) {
                if(image->image_info.vendor == NULL) {
                    image->image_info.vendor = strdup(manifest_schema->annotations->values[it_annotations]);
                }
            } else if(strcmp(RLYEH_ANNOTATION_OCI_VERSION, annotation_key) == 0) {
                if(image->image_info.version == NULL) {
                    image->image_info.version = strdup(manifest_schema->annotations->values[it_annotations]);
                }
            } else if(strcmp(RLYEH_ANNOTATION_OCI_REFNAME, annotation_key) == 0) {
                if(image->image_info.name == NULL) {
                    image->image_info.name = strdup(manifest_schema->annotations->values[it_annotations]);
                }
            }
        }
    }
    char* short_imagename = shorten_image_name(dirname);
    fill_empty_field(short_imagename, image->image_info.version, &image->image_info);
    free(short_imagename);
    image->image_info.disklocation = strdup(dirname);
}

void rlyeh_sync(const char* image_location, const char* storage_location, const char* dirname, rlyeh_sync_data_t* sync_data) {
    parser_error err = NULL;
    image_spec_schema_image_index_schema* index_schema = NULL;
    amxc_string_t index_path;
    amxc_string_init(&index_path, 0);
    amxc_string_setf(&index_path, "%s/%s/index.json", image_location, dirname);

    index_schema = rlyeh_parse_image_spec_schema(index_path.buffer);
    if(!index_schema) {
        SAH_TRACEZ_ERROR(ME, "Index schema not found");
        goto exit;
    }
    if(index_schema->manifests_len) {
        size_t i = 0;
        sync_data->images = (rlyeh_image_data_t**) calloc(1, index_schema->manifests_len * sizeof(rlyeh_image_data_t*));
        sync_data->len = index_schema->manifests_len;

        for(i = 0; i < index_schema->manifests_len; i++) {
            rlyeh_image_data_t* image = (rlyeh_image_data_t*) calloc(1, sizeof(rlyeh_image_data_t));
            init_image_data(image);
            image_spec_schema_image_manifest_schema* manifest_schema = NULL;
            amxc_string_t manifest_name;
            amxc_string_init(&manifest_name, 0);
            get_image_data_from_index(index_schema->manifests[i], image);

            build_manifest_filename_from_index(index_schema, image->image_info.version, storage_location, &manifest_name);
            manifest_schema = image_spec_schema_image_manifest_schema_parse_file(manifest_name.buffer, 0, &err);
            if(!manifest_schema) {
                SAH_TRACEZ_ERROR(ME, "Could not parse manifest [%s] error [%s]", manifest_name.buffer, err);
                sync_data->images[i] = NULL;
                amxc_string_clean(&manifest_name);
                free(err);
                err = NULL;
                continue;

            }
            get_image_data_from_manifest(dirname, manifest_schema, image);

            // fprintf(stderr, "Image : [%s][%s][%s][%s][%s][%s][%d]\n", image->uri, image->uuid, image->name, image->version, image->vendor, image->description, image->markforremoval);

            // if(!check_blobs_size(index_schema->manifests[i], manifest_schema, storage_location)) {
            //     continue;
            // }
            sync_data->images[i] = image;

            free_image_spec_schema_image_manifest_schema(manifest_schema);
            amxc_string_clean(&manifest_name);
            free(err);
            err = NULL;
        }
    }
    free_image_spec_schema_image_index_schema(index_schema);

exit:
    amxc_string_clean(&index_path);
    return;

}
