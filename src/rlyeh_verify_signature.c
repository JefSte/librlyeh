/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <locale.h>

#include <gpgme.h>

#include <yajl/yajl_gen.h>
#include <amxc/amxc.h>
#include <amxj/amxj_variant.h>

#include <rlyeh/rlyeh_utils.h>
#include <rlyeh/rlyeh_curl.h>
#include <rlyeh/rlyeh_verify_signature.h>
#include <rlyeh_verify_signature_priv.h>
#include <rlyeh_priv.h>
#include <rlyeh_assert.h>

#include <debug/sahtrace.h>

#define ME "rlyeh_copy"

#define print_err(err)                    \
    do                                \
    {                               \
        if(err)                          \
        {                           \
            SAH_TRACEZ_ERROR(ME, "%s:%d: %s: %s\n",           \
                             __FILE__, __LINE__, gpgme_strsource(err),   \
                             gpgme_strerror(err));           \
        }                           \
    }                               \
    while(0)

static void init_gpgme (gpgme_protocol_t proto) {
    /* Initialize the locale environment.  */
    gpgme_error_t err;
    gpgme_check_version(NULL);
    setlocale(LC_ALL, "");
    gpgme_set_locale(NULL, LC_CTYPE, setlocale(LC_CTYPE, NULL));

    err = gpgme_engine_check_version(proto);
    print_err(err);
}

static void print_data (gpgme_data_t data, FILE* fd) {
#define BUF_SIZE 512
    char buf[BUF_SIZE + 1];
    int ret;

    ret = gpgme_data_seek(data, 0, SEEK_SET);
    if(ret) {
        print_err(gpgme_err_code_from_errno(errno));
    }
    while((ret = gpgme_data_read(data, buf, BUF_SIZE)) > 0) {
        fwrite(buf, ret, 1, fd);
    }
    if(ret < 0) {
        print_err(gpgme_err_code_from_errno(errno));
    }

    /* Reset read position to the beginning */
    ret = gpgme_data_seek(data, 0, SEEK_SET);
    if(ret) {
        print_err(gpgme_err_code_from_errno(errno));
    }
}

void retrieve_sigstore_and_creds(const char* fname, const char* server, amxc_string_t* usr, amxc_string_t* pwd, amxc_string_t* sigstore) {
    amxc_var_t* sigstore_data = NULL;
    sigstore_data = read_config(fname);
    if(sigstore_data) {
        amxc_var_t* var_server = GET_ARG(sigstore_data, server);
        if(var_server) {
            amxc_var_t* var_sigstore = amxc_var_get_path(var_server, "sigstore", AMXC_VAR_FLAG_DEFAULT);
            amxc_string_set(sigstore, amxc_var_constcast(cstring_t, var_sigstore));
            amxc_var_t* var_usr = amxc_var_get_path(var_server, "username", AMXC_VAR_FLAG_DEFAULT);
            amxc_string_set(usr, amxc_var_constcast(cstring_t, var_usr));
            amxc_var_t* var_pwd = amxc_var_get_path(var_server, "password", AMXC_VAR_FLAG_DEFAULT);
            amxc_string_set(pwd, amxc_var_constcast(cstring_t, var_pwd));

            amxc_var_delete(&var_pwd);
            amxc_var_delete(&var_usr);
            amxc_var_delete(&var_sigstore);
        }
        amxc_var_delete(&var_server);
    }
    amxc_var_delete(&sigstore_data);
}

static int fetch_signature(const rlyeh_signature_data_t* sigdata,
                           const char* sigpath) {
    int ret = 0;
    char* prefix_url = strdup("https://");
    amxc_string_t url;
    amxc_string_t usr;
    amxc_string_t pwd;
    curl_off_t* size = NULL;
    amxc_string_init(&usr, 0);
    amxc_string_init(&pwd, 0);
    amxc_string_init(&url, 0);

    retrieve_sigstore_and_creds("/etc/amx/librlyeh/sigstore.json", sigdata->server, &usr, &pwd, &url);

    amxc_string_set_at(&url, 0, prefix_url, strlen(prefix_url), amxc_string_no_flags);
    amxc_string_appendf(&url, "%s-%s.signature", sigdata->image_name, sigdata->version);

    rlyeh_curl_parameters_t curl_param = {
        .username = &usr,
        .password = &pwd,
        .token = NULL,
        .type = FILE_TYPE_SIG,
        .auth_type = BASIC_AUTHENTICATION
    };

    SAH_TRACEZ_INFO(ME, "Fetch signature %s", url.buffer);

    FILE* fd = fopen(sigpath, "w");
    ret = rlyeh_curl_download_content(&curl_param, &url, (void*) fd, size, NULL);
    fclose(fd);

    amxc_string_clean(&url);
    amxc_string_clean(&usr);
    amxc_string_clean(&pwd);
    free(prefix_url);
    return ret;
}

static gpgme_error_t extract_and_verify_signature(const char* sigpath, const char* jsonname) {
    gpgme_ctx_t ctx;
    gpgme_error_t err;
    gpgme_data_t signature = NULL;
    gpgme_data_t plain = NULL;
    gpgme_verify_result_t vresult;
    FILE* fd = NULL;

    init_gpgme(GPGME_PROTOCOL_OpenPGP);
    err = gpgme_new(&ctx);
    if(err) {
        print_err(err);
        goto exit;
    }

    /* Retrieve data from signature filename to operate on. */
    err = gpgme_data_new_from_file(&signature, sigpath, 1);
    if(err) {
        print_err(err);
        goto exit;
    }

    /* Verify signature. */
    err = gpgme_data_new(&plain);
    if(err) {
        print_err(err);
        goto exit;
    }
    err = gpgme_op_verify(ctx, signature, NULL, plain);
    if(err) {
        print_err(err);
        goto exit;
    }
    vresult = gpgme_op_verify_result(ctx);
    if(!(vresult->signatures->summary & GPGME_SIGSUM_VALID)) {
        err = GPG_ERR_BAD_SIGNATURE;
        SAH_TRACEZ_ERROR(ME, "Signature is not valid");
        goto exit;
    }

    fd = fopen(jsonname, "wb");
    print_data(plain, fd);
    fclose(fd);

exit:
    gpgme_data_release(signature);
    gpgme_data_release(plain);
    gpgme_release(ctx);

    return err;
}

int check_docker_reference(amxc_var_t* obj, const rlyeh_signature_data_t* sigdata) {
    int ret = 0;
    amxc_var_t* var_docker_reference;
    const char* str_docker_reference;
    amxc_string_t expected_docker_reference;
    amxc_string_init(&expected_docker_reference, 0);
    amxc_string_setf(&expected_docker_reference, "%s/%s:%s", sigdata->server, sigdata->image_name, sigdata->version);

    amxc_var_t* identity = GET_ARG(obj, "identity");
    if(!identity) {
        SAH_TRACEZ_ERROR(ME, "Wrong key in json signature");
        ret = 1;
        goto exit;
    }
    var_docker_reference = amxc_var_get_path(identity, "docker-reference", AMXC_VAR_FLAG_DEFAULT);
    str_docker_reference = amxc_var_constcast(cstring_t, var_docker_reference);
    if(strcmp(str_docker_reference, expected_docker_reference.buffer) != 0) {
        SAH_TRACEZ_ERROR(ME, "Invalid value in signature");
        // fprintf(stderr, "docker reference KO \n[%s]\n[%s]\n", str_docker_reference, expected_docker_reference.buffer);
        ret = 1;
        goto exit;
    }

exit:
    amxc_string_clean(&expected_docker_reference);
    return ret;
}

int check_manifest_hash(amxc_var_t* obj, const rlyeh_signature_data_t* sigdata) {
    int ret = 0;
    amxc_var_t* var_docker_manifest_digest;
    const char* str_docker_manifest_digest;
    amxc_string_t expected_manifest_hash;
    amxc_string_init(&expected_manifest_hash, 0);
    amxc_string_setf(&expected_manifest_hash, "sha256:%s", sigdata->manifest_hash);

    amxc_var_t* image = GET_ARG(obj, "image");
    if(!image) {
        SAH_TRACEZ_ERROR(ME, "Wrong key in json signature");
        ret = 1;
        goto exit;
    }
    var_docker_manifest_digest = amxc_var_get_path(image, "docker-manifest-digest", AMXC_VAR_FLAG_DEFAULT);
    str_docker_manifest_digest = amxc_var_constcast(cstring_t, var_docker_manifest_digest);
    if(strcmp(str_docker_manifest_digest, expected_manifest_hash.buffer) != 0) {
        SAH_TRACEZ_ERROR(ME, "Invalid value in signature");
        // fprintf(stderr, "manifest hash KO \n[%s]\n[%s]\n", str_docker_manifest_digest, expected_manifest_hash.buffer);
        ret = 1;
        goto exit;
    }

exit:
    amxc_string_clean(&expected_manifest_hash);
    return ret;
}

int check_type(amxc_var_t* obj) {
    int ret = 0;
    char* expected_type = strdup("atomic container signature");

    amxc_var_t* var_type = amxc_var_get_path(obj, "type", AMXC_VAR_FLAG_DEFAULT);
    const char* str_type = amxc_var_constcast(cstring_t, var_type);
    if(strcmp(str_type, expected_type) != 0) {
        SAH_TRACEZ_ERROR(ME, "Invalid value in signature");
        // fprintf(stderr, "Types KO \n[%s]\n[%s]\n", str_type, expected_type);
        ret = 1;
        goto exit;
    }

exit:
    free(expected_type);
    return ret;
}

int signature_content_verification(const char* jsonfile, const rlyeh_signature_data_t* sigdata) {
    int ret = 0;
    amxc_var_t* config_data = NULL;
    amxc_var_t* critical = NULL;

    config_data = read_config(jsonfile);
    if(!config_data) {
        SAH_TRACEZ_ERROR(ME, "Can't parse signature");
        ret = 1;
        goto exit;
    }
    // Parse data here and compare to docker reference and digest
    critical = GET_ARG(config_data, "critical");
    if(!critical) {
        SAH_TRACEZ_ERROR(ME, "Wrong key in json signature");
        ret = 1;
        goto exit;
    }

    ret = check_docker_reference(critical, sigdata);
    when_failed(ret, exit);

    ret = check_manifest_hash(critical, sigdata);
    when_failed(ret, exit);

    ret = check_type(critical);
    when_failed(ret, exit);

exit:
    amxc_var_delete(&config_data);

    return ret;
}


void rlyeh_signature_data_init(rlyeh_signature_data_t* data) {
    data->uri = NULL;
    data->transport = NULL;
    data->server = NULL;
    data->image_name = NULL;
    data->version = NULL;
    data->manifest_hash = NULL;
}

void rlyeh_signature_data_clean(rlyeh_signature_data_t* data) {
    if(!data) {
        return;
    }
    free(data->uri);
    free(data->transport);
    free(data->server);
    free(data->image_name);
    free(data->version);
    free(data->manifest_hash);
}

int rlyeh_verify_signature(const rlyeh_signature_data_t* sigdata) {
    int ret = 0;
    char* localsigpath = strdup("/tmp/signature");
    char* localjsonname = strdup("/tmp/sigdata.json");

    // Fetch signature https://<sigstore>/<image-name>:<version>.signature
    ret = fetch_signature(sigdata, localsigpath);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Can't fetch the signature for image %s", sigdata->image_name);
        goto exit;
    }

    // Verify signature
    // public key previously (and manually) verified with good trust and validity
    ret = extract_and_verify_signature(localsigpath, localjsonname);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Can't extract signature");
        goto exit;
    }

    // Check manifest signature
    // docker reference
    // manifest digest
    ret = signature_content_verification(localjsonname, sigdata);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Invalid signature");
        goto exit;
    }

exit:
    // Delete signature
    if(file_exists(localsigpath)) {
        remove(localsigpath);
    }
    if(file_exists(localjsonname)) {
        remove(localjsonname);
    }
    free(localsigpath);
    free(localjsonname);

    return ret;
}