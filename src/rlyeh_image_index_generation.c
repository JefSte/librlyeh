/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>

#include <rlyeh/rlyeh_image_index_generation.h>
#include <rlyeh/rlyeh_utils.h>
#include <rlyeh_priv.h>
#include <rlyeh_defines_priv.h>
#include <libocispec/image_spec_schema_image_index_schema.h>
#include <amxc/amxc_string.h>
#include <debug/sahtrace.h>

#define ME "rlyeh_copy"

static void make_manifest_element(rlyeh_index_generation_data_t* data, image_spec_schema_image_index_schema_manifests_element* manifest) {
    json_map_string_string* annotation = (json_map_string_string*) calloc(1, sizeof(*annotation));
    if(data) {
        append_json_map_string_string(annotation, RLYEH_ANNOTATION_OCI_REFNAME, data->version);
        append_json_map_string_string(annotation, RLYEH_ANNOTATION_SAH_URI, data->uri);
        append_json_map_string_string(annotation, RLYEH_ANNOTATION_SAH_UUID, data->uuid);
        append_json_map_string_string(annotation, RLYEH_ANNOTATION_SAH_MAKRFORREMOVAL, data->mark_for_removal ? "1" : "0");
        manifest->digest = (char*) malloc(strlen("sha256:") + strlen(data->manifest_digest) + 1);
        strcpy(manifest->digest, "sha256:");
        strcat(manifest->digest, data->manifest_digest);
        manifest->media_type = strdup("application/vnd.oci.image.manifest.v1+json");
        manifest->size_present = 1;
        manifest->size = data->manifest_size;
        manifest->annotations = annotation;
        manifest->urls_len = 0;
    }

    return;
}

// Check if the index is being generated to add a manifest or to edit an annotation
// It returns the index of the manifest begin updated, or -1.
static int update_annotation_mode(rlyeh_index_generation_data_t* data, image_spec_schema_image_index_schema* current_index_schema) {
    int it;
    bool version = false;
    bool uuid = false;

    for(it = 0; it < (int) current_index_schema->manifests_len; it++) {
        size_t it_annotation;
        for(it_annotation = 0; it_annotation < current_index_schema->manifests[it]->annotations->len; it_annotation++) {
            if(strcmp(RLYEH_ANNOTATION_SAH_UUID, current_index_schema->manifests[it]->annotations->keys[it_annotation]) == 0) {
                if(data->uuid) {
                    if(strcmp(data->uuid, current_index_schema->manifests[it]->annotations->values[it_annotation]) == 0) {
                        uuid = true;
                    }
                }
            }
            if(strcmp(RLYEH_ANNOTATION_OCI_REFNAME, current_index_schema->manifests[it]->annotations->keys[it_annotation]) == 0) {
                if(data->version) {
                    if(strcmp(data->version, current_index_schema->manifests[it]->annotations->values[it_annotation]) == 0) {
                        version = true;
                    }
                }
            }
            if(uuid && version) {
                return it;
            }
        }
    }
    return -1;
}

// Make new index schema and returns the index of the manifest added/updated
static int make_index_schema(rlyeh_index_generation_data_t* data, image_spec_schema_image_index_schema* current_index_schema, image_spec_schema_image_index_schema* new_index_schema) {
    int ret_index = -1;
    int it_manifest_updated = -1;
    int it = 0;
    size_t manifests_len = 0;
    image_spec_schema_image_index_schema_manifests_element** ptr_manifests = NULL;
    image_spec_schema_image_index_schema_manifests_element* manifest = (image_spec_schema_image_index_schema_manifests_element*) calloc(1, sizeof(*manifest));

    if(current_index_schema) {
        it_manifest_updated = update_annotation_mode(data, current_index_schema);
        if(it_manifest_updated != -1) {
            manifests_len = current_index_schema->manifests_len;
        } else {
            manifests_len = current_index_schema->manifests_len + 1;
        }
    } else {
        manifests_len = 1;
    }

    ptr_manifests = (image_spec_schema_image_index_schema_manifests_element**) calloc(1, manifests_len * sizeof(*ptr_manifests));
    make_manifest_element(data, manifest);

    if(current_index_schema) {
        for(it = 0; it < (int) current_index_schema->manifests_len; it++) {
            ptr_manifests[it] = current_index_schema->manifests[it];
        }
    }

    if(current_index_schema && (it_manifest_updated != -1)) {
        ptr_manifests[it_manifest_updated] = manifest;
        ret_index = it_manifest_updated;
    } else {
        ptr_manifests[manifests_len - 1] = manifest;
        ret_index = manifests_len - 1;
    }

    new_index_schema->schema_version_present = 1;
    new_index_schema->schema_version = 2;
    new_index_schema->manifests_len = manifests_len;
    new_index_schema->manifests = ptr_manifests;

    return ret_index;
}

static void free_manifest_element(image_spec_schema_image_index_schema_manifests_element* manifest) {
    if(manifest) {
        free_json_map_string_string(manifest->annotations);
        free(manifest->digest);
        free(manifest->media_type);
        free(manifest);
    }
}

static void fill_rlyeh_index_generation_data(image_spec_schema_image_index_schema_manifests_element* manifest, rlyeh_index_generation_data_t* new_data) {
    size_t it_annotation;
    for(it_annotation = 0; it_annotation < manifest->annotations->len; it_annotation++) {
        if(strcmp(RLYEH_ANNOTATION_OCI_REFNAME, manifest->annotations->keys[it_annotation]) == 0) {
            new_data->version = manifest->annotations->values[it_annotation];
        } else if(strcmp(RLYEH_ANNOTATION_SAH_URI, manifest->annotations->keys[it_annotation]) == 0) {
            new_data->uri = manifest->annotations->values[it_annotation];
        } else if(strcmp(RLYEH_ANNOTATION_SAH_UUID, manifest->annotations->keys[it_annotation]) == 0) {
            new_data->uuid = manifest->annotations->values[it_annotation];
        }
    }

    char* cpy_digest = strdup(manifest->digest);
    char* saveptr = NULL;
    strtok_r(cpy_digest, ":", &saveptr);
    char* shrink_digest = strtok_r(NULL, ":", &saveptr);
    for(size_t i = 0; i < 65; i++) {
        new_data->manifest_digest[i] = *(shrink_digest + i);
    }
    new_data->manifest_digest[64] = 0;

    if(manifest->size_present) {
        new_data->manifest_size = manifest->size;
    }

    free(cpy_digest);
}

static void free_index_schema(image_spec_schema_image_index_schema* index, int modified_manifest) {
    if(index) {
        free_manifest_element(index->manifests[modified_manifest]);
        free(index->manifests);
        free(index);
    }
}

char* rlyeh_image_index_generate_content(rlyeh_index_generation_data_t* data) {
    char* ret = NULL;
    parser_error err = NULL;
    amxc_string_t index_filename;
    image_spec_schema_image_index_schema* current_index_schema = NULL;
    image_spec_schema_image_index_schema* new_index_schema = (image_spec_schema_image_index_schema*) calloc(1, sizeof(*new_index_schema));
    amxc_string_init(&index_filename, 0);
    amxc_string_setf(&index_filename, "%s/index.json", data->image_name);

    current_index_schema = rlyeh_parse_image_spec_schema(index_filename.buffer);
    int modified_manifest = make_index_schema(data, current_index_schema, new_index_schema);
    ret = image_spec_schema_image_index_schema_generate_json(new_index_schema, 0, &err);
    if(current_index_schema) {
        free_image_spec_schema_image_index_schema(current_index_schema);
    }
    free(err);
    free_index_schema(new_index_schema, modified_manifest);
    amxc_string_clean(&index_filename);

    return ret;
}

void rlyeh_write_index(const char* filename, const char* content) {
    FILE* fd = NULL;

    fd = fopen(filename, "w");
    fwrite(content, strlen(content), 1, fd);

    fclose(fd);
}

// Only the modification of the mark for removal is implemented.
void rlyeh_update_index_annotations(const char* image_location, char* image_name, const char* uuid, const char* version, UNUSED const char* annotation_label, const char* value) {
    char* index_content = NULL;
    char* image_path = NULL;
    rlyeh_index_generation_data_t new_data;
    rlyeh_index_generation_data_init(&new_data);
    image_spec_schema_image_index_schema* index_schema = NULL;
    amxc_string_t index_path;
    amxc_string_init(&index_path, 0);
    amxc_string_setf(&index_path, "%s/%s/index.json", image_location, image_name);
    index_schema = rlyeh_parse_image_spec_schema(index_path.buffer);
    if(!index_schema) {
        SAH_TRACEZ_ERROR(ME, "Index schema not found [%s]", index_path.buffer);
        goto exit;
    }

    image_path = (char*) malloc(strlen(image_location) + strlen(image_name) + 2);
    strcpy(image_path, image_location);
    strcat(image_path, "/");
    strcat(image_path, image_name);

    size_t it_man;
    for(it_man = 0; it_man < index_schema->manifests_len; it_man++) {
        size_t it_annotation;
        bool b_uuid = false;
        bool b_version = false;
        for(it_annotation = 0; it_annotation < index_schema->manifests[it_man]->annotations->len; it_annotation++) {
            if(strcmp(RLYEH_ANNOTATION_SAH_UUID, index_schema->manifests[it_man]->annotations->keys[it_annotation]) == 0) {
                if(strcmp(uuid, index_schema->manifests[it_man]->annotations->values[it_annotation]) == 0) {
                    b_uuid = true;
                }
            }
            if(strcmp(RLYEH_ANNOTATION_OCI_REFNAME, index_schema->manifests[it_man]->annotations->keys[it_annotation]) == 0) {
                if(strcmp(version, index_schema->manifests[it_man]->annotations->values[it_annotation]) == 0) {
                    b_version = true;
                }
            }
        }
        if(b_uuid && b_version) {
            new_data.mark_for_removal = (value ? "1" : "0");
            fill_rlyeh_index_generation_data(index_schema->manifests[it_man], &new_data);
        }
    }
    new_data.image_name = image_path;
    index_content = rlyeh_image_index_generate_content(&new_data);

    rlyeh_write_index(index_path.buffer, index_content);
    SAH_TRACEZ_INFO(ME, "Updating index.json");

exit:
    free_image_spec_schema_image_index_schema(index_schema);
    free(index_content);
    free(image_path);
    amxc_string_clean(&index_path);
}

void rlyeh_index_generation_data_init(rlyeh_index_generation_data_t* data) {
    data->image_name = NULL;
    data->manifest_digest[0] = 0;
    data->uri = NULL;
    data->uuid = NULL;
    data->version = NULL;
    data->mark_for_removal = false;
    data->manifest_size = 0;
}