/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <unistd.h>

#include <rlyeh/rlyeh.h>
#include <rlyeh_priv.h>
#include <rlyeh_defines_priv.h>

#include <debug/sahtrace.h>

#include <libocispec/image_spec_schema_image_index_schema.h>
#include <libocispec/image_spec_schema_image_manifest_schema.h>

#include <dirent.h>

#define ME "rlyeh_remove"

static void remove_blob(const char* digest, const char* storageLocation) {
    amxc_string_t fname;
    amxc_string_init(&fname, 0);

    build_fname_from_digest(storageLocation, digest, &fname);
    SAH_TRACEZ_INFO(ME, "Removing blob %s", fname.buffer);

    if(file_exists(fname.buffer)) {
        remove(fname.buffer);
    } else {
        SAH_TRACEZ_WARNING(ME, "Blob %.32s... doesn't exist", fname.buffer);
    }
    amxc_string_clean(&fname);
}

static void remove_oci_layout(const char* imagedir) {
    amxc_string_t oci_layout_filename;
    amxc_string_init(&oci_layout_filename, 0);
    amxc_string_setf(&oci_layout_filename, "%s/oci-layout", imagedir);

    if(file_exists(oci_layout_filename.buffer)) {
        SAH_TRACEZ_INFO(ME, "Removing oci-layout %s", oci_layout_filename.buffer);
        remove(oci_layout_filename.buffer);
    }

    amxc_string_clean(&oci_layout_filename);

    return;
}

static bool is_empty_dir(const char* dirname) {
    int n = 0;
    DIR* dir = opendir(dirname);
    if(dir == NULL) {
        return 1;
    }
    while(readdir(dir) != NULL) {
        if(++n > 2) {
            break;
        }
    }
    closedir(dir);
    if(n <= 2) {
        return true;
    } else {
        return false;
    }
}

static void remove_blobs_dir(const rlyeh_remove_data_t* data, const char* blobsdir) {
    amxc_string_t shadir;
    amxc_string_init(&shadir, 0);
    amxc_string_setf(&shadir, "%s/sha256", blobsdir);
    if(is_empty_dir(shadir.buffer)) {
        rmdir(shadir.buffer);
    }
    if(data->storageLocation == NULL) { // blobs directory is specific to this image
        SAH_TRACEZ_INFO(ME, "Removing blobs directory %s", blobsdir);
        rmdir(blobsdir);
    }
    amxc_string_clean(&shadir);
}

static void update_index(const char* index_filename, const image_spec_schema_image_index_schema* index_schema, const char* version) {
    // Update manifests and manifest_len in index.json
    size_t manifests_len = index_schema->manifests_len - 1;
    parser_error err = NULL;
    image_spec_schema_image_index_schema* new_index_schema = (image_spec_schema_image_index_schema*) calloc(1, sizeof(*new_index_schema));
    image_spec_schema_image_index_schema_manifests_element** ptr_manifests = (image_spec_schema_image_index_schema_manifests_element**) calloc(1, manifests_len * sizeof(*ptr_manifests));
    int it = 0;
    int it_ptr = 0;
    for(it = 0; it < (int) index_schema->manifests_len; it++) {
        if(index_schema->manifests[it]->annotations) {
            size_t it_annotations;
            for(it_annotations = 0; it_annotations < index_schema->manifests[it]->annotations->len; it_annotations++) {
                char* annotation_key = index_schema->manifests[it]->annotations->keys[it_annotations];
                if(strcmp(RLYEH_ANNOTATION_OCI_REFNAME, annotation_key) == 0) {
                    if(strcmp(version, index_schema->manifests[it]->annotations->values[it_annotations]) != 0) {
                        ptr_manifests[it_ptr++] = index_schema->manifests[it];
                    }
                }
            }
        }
    }
    new_index_schema->schema_version_present = 1;
    new_index_schema->schema_version = 2;
    new_index_schema->manifests_len = manifests_len;
    new_index_schema->manifests = ptr_manifests;

    char* ret = image_spec_schema_image_index_schema_generate_json(new_index_schema, 0, &err);

    rlyeh_write_index(index_filename, ret);

    free(new_index_schema);
    free(ret);
    free(ptr_manifests);
    free(err);
}


// This function removes from the amxc_llist_t* layers the layers that are shared with the manifest described by the man_digest.
static void remove_shared_blobs_from_llist(const rlyeh_remove_data_t* data, const char* man_digest, amxc_llist_t* layers) {
    parser_error err = NULL;
    amxc_string_t manifest_filename;
    amxc_string_init(&manifest_filename, 0);

    build_fname_from_digest(data->storageLocation, man_digest, &manifest_filename);

    image_spec_schema_image_manifest_schema* manifest_schema = NULL;

    if(!file_exists(manifest_filename.buffer)) {
        SAH_TRACEZ_INFO(ME, "Manifest %s not found", manifest_filename.buffer);
        return;
    }
    manifest_schema
        = image_spec_schema_image_manifest_schema_parse_file(manifest_filename.buffer,
                                                             NULL,
                                                             &err);
    if(!manifest_schema) {
        SAH_TRACEZ_ERROR(ME, "Manifest is invalid [%s] [%s]", manifest_filename.buffer, err);
        goto exit;
    }

    amxc_llist_for_each(it, layers) {
        amxc_string_t* layer = amxc_llist_it_get_data(it, amxc_string_t, it);
        if(strcmp(layer->buffer, manifest_schema->config->digest) == 0) {
            SAH_TRACEZ_INFO(ME, "Blob %s is shared, do not remove it", layer->buffer);
            amxc_llist_it_clean(it, amxc_string_list_it_free);
        } else if(manifest_schema->layers_len) {
            for(size_t it_layer = 0; it_layer < manifest_schema->layers_len; it_layer++) {
                if(strcmp(layer->buffer, manifest_schema->layers[it_layer]->digest) == 0) {
                    SAH_TRACEZ_INFO(ME, "Blob %s is shared, do not remove it", layer->buffer);
                    amxc_llist_it_clean(it, amxc_string_list_it_free);
                    break;
                }
            }
        }
    }
exit:
    free(err);
    amxc_string_clean(&manifest_filename);
    free_image_spec_schema_image_manifest_schema(manifest_schema);
}

static void find_shared_layers(const rlyeh_remove_data_t* data, amxc_string_t* manifest_ref, amxc_llist_t* llayers, bool* shared_manifest) {
    struct dirent* de;
    DIR* dr = opendir(data->imageLocation);
    if(dr != NULL) {
        while((de = readdir(dr)) != NULL) {
            if((strcmp(de->d_name, ".") == 0) || (strcmp(de->d_name, "..") == 0)) {
                continue;
            }
            image_spec_schema_image_index_schema* index_schema = NULL;
            amxc_string_t index_path;
            amxc_string_init(&index_path, 0);
            amxc_string_setf(&index_path, "%s/%s/index.json", data->imageLocation, de->d_name);


            index_schema = rlyeh_parse_image_spec_schema(index_path.buffer);
            if(!index_schema) {
                SAH_TRACEZ_ERROR(ME, "Index schema not found");
                amxc_string_clean(&index_path);
                continue;
            }
            // Exclude the current image being removed from the shared blobs research
            size_t it_manifest;
            for(it_manifest = 0; it_manifest < index_schema->manifests_len; it_manifest++) {
                // Shared manifest => same images so each blob is shared
                if(strcmp(manifest_ref->buffer, index_schema->manifests[it_manifest]->digest) == 0) {
                    SAH_TRACEZ_INFO(ME, "Manifest is shared, each blob is shared. Do not remove them");
                    *shared_manifest = true;
                    amxc_llist_for_each(it, llayers) {
                        amxc_llist_it_clean(it, amxc_string_list_it_free);
                    }
                    break;
                }
                if(index_schema->manifests[it_manifest]->annotations) {
                    remove_shared_blobs_from_llist(data, index_schema->manifests[it_manifest]->digest, llayers);
                }
            }
            amxc_string_clean(&index_path);
            free_image_spec_schema_image_index_schema(index_schema);
        }
        closedir(dr);
    }
}

static void remove_blobs(const rlyeh_remove_data_t* data, const amxc_string_t* manifest_filename, const amxc_string_t* blobsdir) {
    parser_error err = NULL;
    bool shared_manifest = false;
    amxc_string_t manifest_blob;
    image_spec_schema_image_manifest_schema* manifest_schema = NULL;

    amxc_string_init(&manifest_blob, 0);
    amxc_string_setf(&manifest_blob, "sha256:%s", strrchr(manifest_filename->buffer, '/') + 1);

    if(!file_exists(manifest_filename->buffer)) {
        SAH_TRACEZ_INFO(ME, "Manifest %s not found", manifest_filename->buffer);
        goto exit;
    }
    manifest_schema
        = image_spec_schema_image_manifest_schema_parse_file(manifest_filename->buffer,
                                                             NULL,
                                                             &err);

    if(!check_manifest_validity(manifest_schema, err)) {
        // Build list with layers,
        // then parse other images and delete the shared layers from the list

        // Build list
        amxc_llist_t llayers;
        amxc_llist_init(&llayers);
        amxc_llist_add_string(&llayers, manifest_schema->config->digest);
        if(manifest_schema->layers_len != 0) {
            size_t i;
            for(i = 0; i < manifest_schema->layers_len; i++) {
                amxc_llist_add_string(&llayers, manifest_schema->layers[i]->digest);
            }
        } else {
            SAH_TRACEZ_WARNING(ME, "No layer in the manifest");
        }

        // Find shared layer in image directories
        find_shared_layers(data, &manifest_blob, &llayers, &shared_manifest);

        amxc_llist_for_each(it, &llayers) {
            amxc_string_t* str = amxc_llist_it_get_data(it, amxc_string_t, it);
            remove_blob(str->buffer, blobsdir->buffer);
        }
        amxc_llist_clean(&llayers, amxc_string_list_it_free);
    }

    // Remove manifest
    if(!shared_manifest) {
        SAH_TRACEZ_INFO(ME, "Removing manifest %s", manifest_filename->buffer);
        remove(manifest_filename->buffer);
    }

exit:
    free(err);
    if(manifest_schema) {
        free_image_spec_schema_image_manifest_schema(manifest_schema);
    }
    amxc_string_clean(&manifest_blob);
}

static void build_blobs_list(const char* storagepath, amxc_llist_t* lblobs) {
    struct dirent* de;
    DIR* dr;
    dr = opendir(storagepath);
    if(dr != NULL) {
        while((de = readdir(dr)) != NULL) {
            if((strcmp(de->d_name, ".") == 0) || (strcmp(de->d_name, "..") == 0)) {
                continue;
            }
            struct dirent* sub_de;
            DIR* sub_dr;
            amxc_string_t blobsdir;
            amxc_string_init(&blobsdir, 0);
            amxc_string_setf(&blobsdir, "%s/%s", storagepath, de->d_name);
            sub_dr = opendir(blobsdir.buffer);
            if(sub_dr != NULL) {
                while((sub_de = readdir(sub_dr)) != NULL) {
                    if((strcmp(sub_de->d_name, ".") == 0) || (strcmp(sub_de->d_name, "..") == 0)) {
                        continue;
                    }
                    amxc_string_t blob_digest;
                    amxc_string_init(&blob_digest, 0);
                    amxc_string_setf(&blob_digest, "%s:%s", de->d_name, sub_de->d_name);

                    amxc_llist_add_string(lblobs, blob_digest.buffer);

                    amxc_string_clean(&blob_digest);
                }
                closedir(sub_dr);
            }
            amxc_string_clean(&blobsdir);
        }
        closedir(dr);
    }

}

static int take_used_blobs_from_list(const char* storagepath, const char* imagepath, const char* dirname, amxc_llist_t* lblobs) {
    int ret = -1;
    parser_error err = NULL;
    image_spec_schema_image_index_schema* index_schema = NULL;
    size_t it_manifest;
    amxc_string_t index;
    amxc_string_init(&index, 0);
    amxc_string_setf(&index, "%s/%s/index.json", imagepath, dirname);

    index_schema = rlyeh_parse_image_spec_schema(index.buffer);
    if(!index_schema) {
        SAH_TRACEZ_ERROR(ME, "Index schema not found [%s]", index.buffer);
        goto exit;
    }

    for(it_manifest = 0; it_manifest < index_schema->manifests_len; it_manifest++) {
        if(index_schema->manifests[it_manifest]->annotations) {
            amxc_string_t manifest_filename;
            amxc_string_init(&manifest_filename, 0);

            build_fname_from_digest(storagepath, index_schema->manifests[it_manifest]->digest, &manifest_filename);
            if(!file_exists(manifest_filename.buffer)) {
                SAH_TRACEZ_ERROR(ME, "No manifest in image %s", dirname);
                amxc_string_clean(&manifest_filename);
                goto exit;
            }
            image_spec_schema_image_manifest_schema* manifest_schema = NULL;

            manifest_schema = image_spec_schema_image_manifest_schema_parse_file(manifest_filename.buffer,
                                                                                 NULL,
                                                                                 &err);
            if(!manifest_schema) {
                SAH_TRACEZ_ERROR(ME, "Manifest is invalid [%s] [%s]", manifest_filename.buffer, err);
                amxc_string_clean(&manifest_filename);
                free_image_spec_schema_image_manifest_schema(manifest_schema);
                free(err);
                err = NULL;
                continue;

            }

            amxc_llist_for_each(it, lblobs) {
                amxc_string_t* digest = amxc_llist_it_get_data(it, amxc_string_t, it);
                if(strcmp(digest->buffer, manifest_schema->config->digest) == 0) {
                    amxc_llist_it_clean(it, amxc_string_list_it_free);
                } else if(strcmp(digest->buffer, index_schema->manifests[it_manifest]->digest) == 0) {
                    amxc_llist_it_clean(it, amxc_string_list_it_free);
                } else if(manifest_schema->layers_len) {
                    for(size_t it_layer = 0; it_layer < manifest_schema->layers_len; it_layer++) {
                        if(strcmp(digest->buffer, manifest_schema->layers[it_layer]->digest) == 0) {
                            amxc_llist_it_clean(it, amxc_string_list_it_free);
                            break;
                        }
                    }
                }
            }
            free(err);
            err = NULL;
            amxc_string_clean(&manifest_filename);
            free_image_spec_schema_image_manifest_schema(manifest_schema);
        }
    }
    ret = 0;
exit:
    if(index_schema) {
        free_image_spec_schema_image_index_schema(index_schema);
    }
    free(err);
    amxc_string_clean(&index);

    return ret;
}

static int recursive_search_for_in_use_blobs(const char* storageLocation, const char* imageLocation, const char* dirname, amxc_llist_t* lblobs) {
    int status = 0;
    struct dirent* de;
    amxc_string_t dname;
    amxc_string_init(&dname, 0);
    amxc_string_setf(&dname, "%s/%s", imageLocation, dirname);

    DIR* dr = opendir(dname.buffer);
    if(dr != NULL) {
        while((de = readdir(dr)) != NULL) {
            if((strcmp(de->d_name, ".") == 0) || (strcmp(de->d_name, "..") == 0)) {
                continue;
            }
            if(de->d_type == DT_DIR) {
                amxc_string_t iname;
                amxc_string_init(&iname, 0);
                amxc_string_setf(&iname, "%s/%s", dirname, de->d_name);
                status = recursive_search_for_in_use_blobs(storageLocation, imageLocation, iname.buffer, lblobs);
                amxc_string_clean(&iname);
                if(status != 0) {
                    break;
                }
            } else {
                status = take_used_blobs_from_list(storageLocation, imageLocation, dirname, lblobs);
                break;
            }
        }
        closedir(dr);
    }
    amxc_string_clean(&dname);
    return status;
}


static void search_for_in_use_blobs(const char* storagepath, const char* imagepath, amxc_llist_t* lblobs) {
    struct dirent* de;
    DIR* dr;
    dr = opendir(imagepath);
    if(dr != NULL) {
        while((de = readdir(dr)) != NULL) {
            if((strcmp(de->d_name, ".") == 0) || (strcmp(de->d_name, "..") == 0)) {
                continue;
            }
            int status = recursive_search_for_in_use_blobs(storagepath, imagepath, de->d_name, lblobs);
            if(status != 0) {
                break;
            }
        }
        closedir(dr);
    }
}

static void imagespec_item_clean_it(amxc_llist_it_t* it) {
    imagespec_item_t* item = amxc_llist_it_get_data(it, imagespec_item_t, it);
    amxc_string_clean(&item->imagename);
    amxc_llist_clean(&item->versions, amxc_string_list_it_free);
    amxc_llist_it_clean(&item->it, NULL);
    free(item);
}

static void remove_image_spec(const char* imagepath, const char* storagepath, amxc_llist_t* imagespecs) {
    int res;
    amxc_llist_for_each(it, imagespecs) {
        imagespec_item_t* item = amxc_llist_it_get_data(it, imagespec_item_t, it);

        amxc_llist_for_each(itv, &item->versions) {
            amxc_string_t* it_version = amxc_llist_it_get_data(itv, amxc_string_t, it);
            // build remove data and remove the image spec
            rlyeh_remove_data_t data;
            rlyeh_remove_data_init(&data);
            data.storageLocation = strdup(storagepath);
            data.imageLocation = strdup(imagepath);
            data.name = strdup(amxc_string_get(&item->imagename, 0));
            data.version = strdup(it_version->buffer);
            SAH_TRACEZ_WARNING(ME, "Deleting orphan image spec (%s:%s)", amxc_string_get(&item->imagename, 0), it_version->buffer);
            res = rlyeh_remove(&data);
            if(res != 0) {
                SAH_TRACEZ_ERROR(ME, "Failed removing orphan image in gc (%d)", res);
            }
            rlyeh_remove_data_clean(&data);
        }
    }
}

static void remove_imagespec_from_llist(amxc_llist_t* imagespecs, const char* imagename, const char* version) {

    if(!imagename || !version) {
        return;
    }

    amxc_llist_for_each(it, imagespecs) {
        imagespec_item_t* item = amxc_llist_it_get_data(it, imagespec_item_t, it);
        if(strcmp(imagename, amxc_string_get(&item->imagename, 0)) != 0) {
            continue;
        }

        amxc_llist_for_each(itv, &item->versions) {
            if(!itv) {
                continue;
            }
            amxc_string_t* it_version = amxc_llist_it_get_data(itv, amxc_string_t, it);
            if(strcmp(version, amxc_string_get(it_version, 0)) != 0) {
                continue;
            }
            // amxc_llist_it_take(&it_version->it);
            amxc_llist_it_clean(&it_version->it, amxc_string_list_it_free);
            // amxc_string_list_it_free(&it_version->it);
        }

        if(amxc_llist_is_empty(&item->versions)) {
            amxc_llist_it_clean(it, imagespec_item_clean_it);
        }
    }
}

static void take_imgspec_from_list(amxc_llist_t* imagespecs, amxc_llist_t* llimages) {
    amxc_llist_for_each(it, llimages) {
        amxc_string_t* image = amxc_llist_it_get_data(it, amxc_string_t, it);
        int pos = amxc_string_search(image, ":", 0) + 1;
        char* version = strdup(amxc_string_get(image, pos));
        amxc_string_shrink(image, amxc_string_buffer_length(image) - pos);
        const char* imagename = amxc_string_get(image, 0);

        remove_imagespec_from_llist(imagespecs, imagename, version);
        free(version);
    }
}

static void fill_versions_llist(image_spec_schema_image_index_schema_manifests_element* manifest, imagespec_item_t* image) {
    size_t index_annotations;
    for(index_annotations = 0; index_annotations < manifest->annotations->len; index_annotations++) {
        if(strcmp(RLYEH_ANNOTATION_OCI_REFNAME, manifest->annotations->keys[index_annotations]) == 0) {
            amxc_llist_add_string(&image->versions, manifest->annotations->values[index_annotations]);
        }
    }
}

static void imagespec_item_init(imagespec_item_t* data) {
    amxc_string_init(&data->imagename, 0);
    amxc_llist_init(&data->versions);
    amxc_llist_it_init(&data->it);
}

static void build_imagespec_list(const char* imagepath, amxc_llist_t* limagespec) {
    struct dirent* de;
    DIR* dr;
    dr = opendir(imagepath);
    if(dr != NULL) {
        while((de = readdir(dr)) != NULL) {
            if((strcmp(de->d_name, ".") == 0) || (strcmp(de->d_name, "..") == 0)) {
                continue;
            }

            amxc_string_t fnameindex;
            image_spec_schema_image_index_schema* index_schema = NULL;
            imagespec_item_t* imagespec = (imagespec_item_t*) calloc(1, sizeof(imagespec_item_t));
            imagespec_item_init(imagespec);

            amxc_string_init(&fnameindex, 0);
            amxc_string_setf(&fnameindex, "%s/%s/index.json", imagepath, de->d_name);


            index_schema = rlyeh_parse_image_spec_schema(fnameindex.buffer);
            if(index_schema) {
                for(size_t i = 0; i < index_schema->manifests_len; i++) {
                    fill_versions_llist(index_schema->manifests[i], imagespec);
                }
                free_image_spec_schema_image_index_schema(index_schema);
            }


            amxc_string_set(&imagespec->imagename, de->d_name);
            amxc_llist_append(limagespec, &(imagespec->it));

            amxc_string_clean(&fnameindex);
        }
    }

    if(dr) {
        closedir(dr);
    }
}

static void rm_image_dir(const char* image_location, amxc_string_t* imgdir) {
    char* pos = NULL;
    while(strcmp(image_location, imgdir->buffer) != 0) {
        if(is_empty_dir(imgdir->buffer)) {
            SAH_TRACEZ_INFO(ME, "Removing %s", imgdir->buffer);
            rmdir(imgdir->buffer);
        }

        // Shrink imgdir to delete empty layers until the root of the image location
        pos = strrchr(imgdir->buffer, '/');
        int length = pos - imgdir->buffer;
        amxc_string_shrink(imgdir, amxc_string_text_length(imgdir) - length);

    }
}

void rlyeh_remove_data_init(rlyeh_remove_data_t* data) {
    data->storageLocation = NULL;
    data->imageLocation = NULL;
    data->name = NULL;
    data->version = NULL;
}

void rlyeh_remove_data_clean(rlyeh_remove_data_t* data) {
    if(!data) {
        return;
    }
    free(data->name);
    free(data->version);
    free(data->storageLocation);
    free(data->imageLocation);
}

int rlyeh_remove(const rlyeh_remove_data_t* data) {
    int rc = -1;
    image_spec_schema_image_index_schema* index_schema = NULL;
    amxc_string_t blobsdir;
    amxc_string_t imagedir;
    amxc_string_t index_filename;
    amxc_string_t manifest_filename;
    amxc_string_init(&blobsdir, 0);
    amxc_string_init(&imagedir, 0);
    amxc_string_init(&index_filename, 0);
    amxc_string_init(&manifest_filename, 0);
    amxc_string_setf(&index_filename, "%s/%s/index.json", data->imageLocation, data->name);
    amxc_string_setf(&imagedir, "%s/%s", data->imageLocation, data->name);

    if(data->storageLocation) {
        amxc_string_set(&blobsdir, data->storageLocation);
    } else {
        amxc_string_setf(&blobsdir, "%s/blobs", imagedir.buffer);
    }

    index_schema = rlyeh_parse_image_spec_schema(index_filename.buffer);
    if(!index_schema) {
        SAH_TRACEZ_ERROR(ME, "Index schema not found");
        goto exit;
    }
    if(!index_schema) {
        SAH_TRACEZ_INFO(ME, "File %s doesn't exist", index_filename.buffer);
        amxc_string_setf(&manifest_filename, "%s/manifest.json", imagedir.buffer);
        // Remove image dir
        remove_oci_layout(imagedir.buffer);
        SAH_TRACEZ_INFO(ME, "Removing image directory %s", imagedir.buffer);
        rmdir(imagedir.buffer);
    } else {

        build_manifest_filename_from_index(index_schema, data->version, blobsdir.buffer, &manifest_filename);

        if(index_schema->manifests_len > 1) { // There is an other version of the image
            update_index(index_filename.buffer, index_schema, data->version);
        } else {                              // Last version of the image to be deleted => delete the image directory
            SAH_TRACEZ_INFO(ME, "Removing index %s", index_filename.buffer);
            remove(index_filename.buffer);

            remove_oci_layout(imagedir.buffer);

            rm_image_dir(data->imageLocation, &imagedir);
        }
        free_image_spec_schema_image_index_schema(index_schema);
    }

    remove_blobs(data, &manifest_filename, &blobsdir);

    remove_blobs_dir(data, blobsdir.buffer);
    rc = 0;
exit:
    amxc_string_clean(&blobsdir);
    amxc_string_clean(&imagedir);
    amxc_string_clean(&index_filename);
    amxc_string_clean(&manifest_filename);
    return rc;
}

int rlyeh_remove_unlisted_blobs(const char* storagepath, const char* imagepath) {
    amxc_llist_t lblobs;
    amxc_llist_init(&lblobs);

    build_blobs_list(storagepath, &lblobs);

    search_for_in_use_blobs(storagepath, imagepath, &lblobs);

    amxc_llist_for_each(it, &lblobs) {
        amxc_string_t* str = amxc_llist_it_get_data(it, amxc_string_t, it);
        remove_blob(str->buffer, storagepath);
    }

    amxc_llist_clean(&lblobs, amxc_string_list_it_free);
    return 0;
}

int rlyeh_remove_unlisted_imagespec(const char* storagepath, const char* imagepath, amxc_llist_t* llimages) {
    amxc_llist_t imagespec;
    amxc_llist_init(&imagespec);
    build_imagespec_list(imagepath, &imagespec);

    take_imgspec_from_list(&imagespec, llimages);

    remove_image_spec(imagepath, storagepath, &imagespec);

    amxc_llist_clean(&imagespec, imagespec_item_clean_it);
    return 0;
}
