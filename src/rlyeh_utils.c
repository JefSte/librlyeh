/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <sys/stat.h>
#include <openssl/sha.h>

#include <rlyeh/rlyeh_utils.h>
#include <rlyeh_priv.h>
#include <rlyeh_assert.h>
#include <rlyeh_defines_priv.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxj/amxj_variant.h>

#include <debug/sahtrace.h>

#define ME "rlyeh_utils"

bool file_exists(const char* filename) {
    bool res = false;
    when_null(filename, exit);
    if(access(filename, F_OK) == 0) {
        res = true;
    }
exit:
    return res;
}

bool dir_exists(const char* dirname) {
    struct stat s;
    bool res = false;
    when_null(dirname, exit);
    if((stat(dirname, &s) != -1) && S_ISDIR(s.st_mode)) {
        res = true;
    }
exit:
    return res;
}

// To know if an image already exists, we need to know if there is a directory with the image name
// AND if the index.json contains the ref.name.
bool image_already_exists(const rlyeh_image_parameters_t* image) {
    bool ret = false;
    when_null(image, exit);
    if(!image->image_name || !image->version) {
        SAH_TRACEZ_INFO(ME, "No image name or version");
        return ret;
    }
    if(dir_exists(image->image_name)) {
        amxc_string_t index_filename;
        amxc_string_init(&index_filename, 0);
        amxc_string_setf(&index_filename, "%s/index.json", image->image_name);
        image_spec_schema_image_index_schema* index_schema = rlyeh_parse_image_spec_schema(index_filename.buffer);
        if(index_schema) {
            size_t it_manifests;
            for(it_manifests = 0; it_manifests < index_schema->manifests_len; it_manifests++) {
                size_t it_annotations;
                for(it_annotations = 0; it_annotations < index_schema->manifests[it_manifests]->annotations->len; it_annotations++) {
                    if((strcmp(RLYEH_ANNOTATION_OCI_REFNAME, index_schema->manifests[it_manifests]->annotations->keys[it_annotations]) == 0)
                       && (strcmp(image->version, index_schema->manifests[it_manifests]->annotations->values[it_annotations]) == 0)) {
                        ret = true;
                    }
                }
            }
            free_image_spec_schema_image_index_schema(index_schema);
        }
        amxc_string_clean(&index_filename);
    }
exit:
    return ret;
}

int rlyeh_image_parameters_init(rlyeh_image_parameters_t* data) {
    int res = -1;
    when_null(data, exit);
    data->image_name = NULL;
    data->transport = NULL;
    data->version = NULL;
    data->auth_type = NO_AUTHENTICATION;
    amxc_string_init(&data->server, 0);
    amxc_string_init(&data->username, 0);
    amxc_string_init(&data->password, 0);
    amxc_string_init(&data->token, 0);
    data->sv = false;
    res = 0;
exit:
    return res;
}

int rlyeh_image_parameters_clean(rlyeh_image_parameters_t* data) {
    int res = -1;
    when_null(data, exit);

    free(data->image_name);
    free(data->transport);
    free(data->version);
    amxc_string_clean(&data->server);
    amxc_string_clean(&data->username);
    amxc_string_clean(&data->password);
    amxc_string_clean(&data->token);
    res = 0;
exit:
    return res;
}

// Parse path : transport:imagename:version
int rlyeh_parse_local_image(const char* data, rlyeh_image_parameters_t* path_parse) {
    int res = -1;
    char* datacpy = NULL;
    char* saveptr = NULL;
    when_null(data, exit);
    when_null(path_parse, exit);

    datacpy = strdup(data);

    path_parse->transport = strdup(strtok_r(datacpy, ":", &saveptr));
    path_parse->image_name = strdup(strtok_r(NULL, ":", &saveptr));
    path_parse->version = strdup(strtok_r(NULL, ":", &saveptr));
    free(datacpy);

    res = 0;
exit:
    return res;
}

// Extract the two last parts of the image name in case of long paths.
char* shorten_image_name(const char* name) {
    static size_t max_len = 64;
    const char* start = name;
    const char* next_slash = NULL;

    // if longer then 64, then cut first part before slash
    while((strlen(start) > max_len) &&
          ((next_slash = strchr(start, '/')) != NULL) &&
          (*(next_slash + 1) != '\0')) {
        start = next_slash + 1;
    }

    if(strlen(start) < max_len) {
        return strdup(start);
    }

    // if still longer, return first 64 chars
    char* ret = (char*) calloc(1, max_len + 1);
    memcpy(ret, start, max_len);
    return ret;
}

// Parse path : transport://server/image_name:version and transport://image_name:version
int rlyeh_parse_uri(const char* uri, rlyeh_image_parameters_t* uri_elements) {
    int res = -1;
    char* server;
    char* rest;
    char* end_server_pos;
    char* version;
    char* saveptr = NULL;
    char* uricpy = NULL;
    when_null(uri, exit);
    when_null(uri_elements, exit);
    uricpy = strdup(uri);
    uri_elements->transport = strdup(strtok_r(uricpy, "://", &saveptr));
    rest = strtok_r(NULL, "\0", &saveptr);
    while(*rest == '/') {
        rest++;
    }

    end_server_pos = strrchr(rest, '/');
    if(end_server_pos) {
        server = strtok_r(rest, "/", &saveptr);
        amxc_string_set(&uri_elements->server, server);
        uri_elements->image_name = strdup(strtok_r(NULL, ":", &saveptr));
    } else {
        uri_elements->image_name = strdup(strtok_r(rest, ":", &saveptr));
    }
    version = strtok_r(NULL, ":", &saveptr);

    if(!version) {
        // No version specified => set to "latest"
        uri_elements->version = strdup("latest");
    } else {
        uri_elements->version = strdup(version);
    }

    // fprintf(stderr, "[%s][%s][%s][%s] \n", uri_elements->transport, uri_elements->server.buffer, uri_elements->image_name, uri_elements->version);
    res = 0;
exit:
    free(uricpy);
    return res;
}

int rlyeh_get_creds_from_cmd(char* data, rlyeh_image_parameters_t* creds_parse) {
    int res = -1;
    char* saveptr = NULL;
    char* username = NULL;
    char* password = NULL;
    when_null(data, exit);
    when_null(creds_parse, exit);
    username = strtok_r(data, ":", &saveptr);
    password = strtok_r(NULL, "\0", &saveptr);
    amxc_string_set(&creds_parse->username, username);
    amxc_string_set(&creds_parse->password, password);
    res = 0;

exit:
    return res;
}

amxc_var_t* read_config(const char* file) {
    int read_length = 0;
    int fd = 0;
    amxc_var_t* var = NULL;
    variant_json_t* reader = NULL;

    fd = open(file, O_RDONLY);
    if(fd < 0) {
        SAH_TRACEZ_INFO(ME, "Could not open file [%s] (%s)", file, strerror(errno));
        goto exit;
    }

    amxj_reader_new(&reader);
    when_null(reader, exit);

    read_length = amxj_read(reader, fd);
    while(read_length > 0) {
        read_length = amxj_read(reader, fd);
    }

    var = amxj_reader_result(reader);

exit:
    if(fd) {
        close(fd);
    }
    amxj_reader_delete(&reader);
    return var;
}

int rlyeh_get_server_from_file(rlyeh_image_parameters_t* param) {
    int res = -1;

    amxc_var_t* config_data = NULL;
    when_null(param, exit);
    config_data = read_config("/etc/amx/librlyeh/registry-auth.json");

    if(config_data) {
        amxc_var_t* credentials = GET_ARG(config_data, "credentials");
        if(credentials) {
            amxc_var_t* var = amxc_var_take_index(credentials, 0);
            amxc_var_t* var_server = amxc_var_get_path(var, "server", AMXC_VAR_FLAG_DEFAULT);
            const char* str_server = amxc_var_constcast(cstring_t, var_server);
            amxc_string_set(&param->server, str_server);
            amxc_var_delete(&var_server);
            amxc_var_delete(&var);
        }
        amxc_var_delete(&credentials);
    }
    amxc_var_delete(&config_data);
    res = 0;
exit:
    return res;

}

int rlyeh_get_creds_from_file(rlyeh_image_parameters_t* param) {
    int res = -1;
    amxc_var_t* config_data = NULL;
    amxc_var_t* credentials = NULL;
    when_null(param, exit);
    config_data = read_config("/etc/amx/librlyeh/registry-auth.json");
    when_null(config_data, exit);
    credentials = GET_ARG(config_data, "credentials");
    if(credentials) {
        amxc_var_for_each(var, credentials) {
            amxc_var_t* var_name = amxc_var_get_path(var, "server", AMXC_VAR_FLAG_DEFAULT);
            const char* str_name = amxc_var_constcast(cstring_t, var_name);
            if(strcmp(param->server.buffer, str_name) == 0) {
                amxc_var_t* var_username = amxc_var_get_path(var, "username", AMXC_VAR_FLAG_DEFAULT);
                const char* str_username = amxc_var_constcast(cstring_t, var_username);
                amxc_var_t* var_password = amxc_var_get_path(var, "password", AMXC_VAR_FLAG_DEFAULT);
                const char* str_password = amxc_var_constcast(cstring_t, var_password);

                amxc_string_set(&param->username, str_username);
                amxc_string_set(&param->password, str_password);

                SAH_TRACEZ_INFO(ME, "Retrieving creds from file");

                amxc_var_delete(&var_username);
                amxc_var_delete(&var_password);
                amxc_var_delete(&var_name);
                res = 0;
                goto exit;
            }
            amxc_var_delete(&var_name);
        }
        SAH_TRACEZ_INFO(ME, "Credentials not found with server %s...", param->server.buffer);
    }

exit:
    amxc_var_delete(&credentials);
    amxc_var_delete(&config_data);
    return res;
}

bool build_manifest_filename_from_index(const image_spec_schema_image_index_schema* index_schema, const char* image_version, const char* storage_location, amxc_string_t* manifest_filename) {
    char* manifest = NULL;
    size_t it_manifest;

    for(it_manifest = 0; it_manifest < index_schema->manifests_len; it_manifest++) {
        if(index_schema->manifests[it_manifest]->annotations) {
            size_t it_annotations;
            for(it_annotations = 0; it_annotations < index_schema->manifests[it_manifest]->annotations->len; it_annotations++) {
                char* annotation_key = index_schema->manifests[it_manifest]->annotations->keys[it_annotations];
                if(strcmp(RLYEH_ANNOTATION_OCI_REFNAME, annotation_key) == 0) {
                    if(strcmp(image_version, index_schema->manifests[it_manifest]->annotations->values[it_annotations]) == 0) {
                        manifest = index_schema->manifests[it_manifest]->digest;
                        break;
                    }
                }
            }
        }
        if(manifest) {
            break;
        }
    }
    when_null(manifest, exit);

    build_fname_from_digest(storage_location, manifest, manifest_filename);

    return false;

exit:
    return true;
}

bool check_manifest_validity(const image_spec_schema_image_manifest_schema* manifest, UNUSED parser_error err) {
    if(manifest == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not parse Image Manifest file, Err: %s", err);
        return true;
    }

    if(manifest->schema_version_present && (manifest->schema_version != 2)) {
        SAH_TRACEZ_ERROR(ME, "The manifest has an invalid schema version.");
        SAH_TRACEZ_ERROR(ME, "Schema version: %d\n", manifest->schema_version);
        return true;
    }

    if(manifest->config == NULL) {
        SAH_TRACEZ_ERROR(ME, "image config is NULL");
        return true;
    }

    // if(strcmp("application/vnd.oci.image.config.v1+json",
    //           manifest->config->media_type) != 0) {
    //     SAH_TRACEZ_ERROR(ME, "Unexpected config type: [%s] Expected [application/vnd.oci.image.config.v1+json]", manifest->config->media_type);
    //     return true;
    // }

    return false;
}

static void sha256_hash_string (unsigned char hash[SHA256_DIGEST_LENGTH], char outputBuffer[65]) {
    int i = 0;

    for(i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        sprintf(outputBuffer + (i * 2), "%02x", hash[i]);
    }

    outputBuffer[64] = 0;
}

int rlyeh_hash_blob(const char* filename, char outputBuffer[65]) {
    int bytes_read = 0;
    FILE* file = fopen(filename, "rb");
    if(!file) {
        return -1;
    }

    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    const int bufSize = 1024;
    unsigned char* buffer = (unsigned char*) malloc(bufSize);
    int bytesRead = 0;
    if(!buffer) {
        return ENOMEM;
    }
    while((bytesRead = fread(buffer, 1, bufSize, file))) {
        SHA256_Update(&sha256, buffer, bytesRead);
        bytes_read += bytesRead;
    }
    SHA256_Final(hash, &sha256);

    sha256_hash_string(hash, outputBuffer);
    fclose(file);
    free(buffer);
    return bytes_read;
}

int complete_uri(const char* uri, amxc_string_t* full_uri) {
    int res = -1;
    rlyeh_image_parameters_t uri_elements;
    when_null(uri, exit);
    when_null(full_uri, exit);
    rlyeh_image_parameters_init(&uri_elements);

    rlyeh_parse_uri(uri, &uri_elements);
    if(amxc_string_is_empty(&uri_elements.server)) {
        rlyeh_get_server_from_file(&uri_elements);
    }
    amxc_string_setf(full_uri, "%s://%s/%s:%s", uri_elements.transport, uri_elements.server.buffer, uri_elements.image_name, uri_elements.version);
    rlyeh_image_parameters_clean(&uri_elements);
    res = 0;
exit:
    return res;

}

int build_fname_from_digest(const char* storage_location, const char* digest, amxc_string_t* fname) {
    int res = -1;
    char* cpy_digest = NULL;
    char* pos = NULL;
    when_null(storage_location, exit);
    when_null(digest, exit);
    when_null(fname, exit);

    cpy_digest = strdup(digest);
    if((pos = strchr(cpy_digest, ':')) != NULL) {
        *pos = '/';
    }
    amxc_string_setf(fname, "%s/%s", storage_location, cpy_digest);
    res = 0;
exit:
    free(cpy_digest);
    return res;

}

int set_err_msg(char* dest, const char* msg, ...) {
    int res = 0;
    if(!dest) {
        return 0;
    }
    va_list myargs;
    va_start(myargs, msg);
    res = vsnprintf(dest, RLYEH_ERR_MSG_LEN - 1, msg, myargs);
    va_end(myargs);
    return res;
}

image_spec_schema_image_index_schema* rlyeh_parse_image_spec_schema(const char* image_name) {
    image_spec_schema_image_index_schema* image_spec_schema = NULL;
    parser_error err = NULL;
    if(!image_name) {
        SAH_TRACEZ_ERROR(ME, "Image name is null");
        goto exit;
    }
    if(!file_exists(image_name)) {
        SAH_TRACEZ_INFO(ME, "File does not exist: %s", image_name);
        goto exit;
    }
    image_spec_schema = image_spec_schema_image_index_schema_parse_file(image_name, 0, &err);
    if(!image_spec_schema) {
        SAH_TRACEZ_ERROR(ME, "Index schema is invalid: %s", err ? err :"???");
        SAH_TRACEZ_ERROR(ME, "It will be removed");
        unlink(image_name);
    }
exit:
    free(err);
    return image_spec_schema;
}
