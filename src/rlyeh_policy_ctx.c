/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>

#include <rlyeh/rlyeh_utils.h>
#include <rlyeh/rlyeh_policy_ctx.h>

#include <yajl/yajl_gen.h>
#include <amxc/amxc.h>
#include <amxj/amxj_variant.h>

#include <debug/sahtrace.h>

#define ME "rlyeh_copy"

rlyeh_policy_ctx_t rlyeh_read_policy(const char* fname, const char* transport, const char* server) {
    amxc_var_t* policy_data = NULL;
    rlyeh_policy_ctx_t ret = NO_SPECIFIED;
    policy_data = read_config(fname);
    if(policy_data) {
        // Parse data here and compare to docker reference and digest
        amxc_var_t* transport_list = GET_ARG(policy_data, "transports");
        if(transport_list) {
            amxc_var_t* var_transport = GET_ARG(transport_list, transport);
            if(var_transport) {
                amxc_var_t* var_server = GET_ARG(var_transport, server);
                if(var_server) {
                    amxc_var_for_each(var, var_server) {
                        amxc_var_t* var_type = amxc_var_get_path(var, "type", AMXC_VAR_FLAG_DEFAULT);
                        const char* str_type = amxc_var_constcast(cstring_t, var_type);
                        amxc_var_t* var_key_type = amxc_var_get_path(var, "keyType", AMXC_VAR_FLAG_DEFAULT);
                        const char* str_key_type = amxc_var_constcast(cstring_t, var_key_type);
                        if(strcmp(str_type, "signedBy") == 0) {
                            if(strcmp(str_key_type, "GPGKeys") == 0) {
                                ret = GPG_KEY_SIGNATURE;
                            }
                        } else if(strcmp(str_type, "insecureAcceptAnything") == 0) {
                            ret = ACCEPT_ANYTHING;
                        }
                        amxc_var_delete(&var_key_type);
                        amxc_var_delete(&var_type);
                    }
                }
                amxc_var_delete(&var_server);
            }
            amxc_var_delete(&var_transport);
        }
        amxc_var_delete(&transport_list);
        if(ret == NO_SPECIFIED) {
            amxc_var_t* var_default = GET_ARG(policy_data, "default");
            if(var_default) {
                amxc_var_for_each(var, var_default) {
                    amxc_var_t* var_type = amxc_var_get_path(var, "type", AMXC_VAR_FLAG_DEFAULT);
                    const char* str_type = amxc_var_constcast(cstring_t, var_type);
                    if(strcmp(str_type, "insecureAcceptAnything") == 0) {
                        ret = ACCEPT_ANYTHING;
                    }
                    amxc_var_delete(&var_type);
                }
            }
            amxc_var_delete(&var_default);
        }
    }
    amxc_var_delete(&policy_data);

    return ret;
}
