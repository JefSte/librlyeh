/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>

#include <rlyeh/rlyeh_utils.h>
#include <rlyeh/rlyeh_image_info.h>
#include <rlyeh_priv.h>
#include <rlyeh_assert.h>
#include <rlyeh_defines_priv.h>
#include <libocispec/image_spec_schema_image_index_schema.h>
#include <libocispec/image_spec_schema_image_manifest_schema.h>

#define ME "rlyeh_utils"

void fill_empty_field(const char* image_name, const char* version, rlyeh_image_info_t* data) {
    if(data->description == NULL) {
        data->description = strdup("");
    }
    if(data->vendor == NULL) {
        data->vendor = strdup("");
    }

    // !! Don't forget we suppose right now that
    // org.opencontainers.image.ref.name should be the same as the physical bundle name
    if(data->name == NULL) {
        data->name = strdup(image_name);
    }

    if(data->version == NULL) {
        // Put the version from the index.json to export
        // the manifest version however has priority
        data->version = strdup(version);
    }
}

void rlyeh_image_info_init(rlyeh_image_info_t* data) {
    if(!data) {
        return;
    }
    data->name = NULL;
    data->disklocation = NULL;
    data->vendor = NULL;
    data->version = NULL;
    data->description = NULL;
}

void rlyeh_image_info_clean(rlyeh_image_info_t* data) {
    if(!data) {
        return;
    }
    free(data->name);
    free(data->disklocation);
    free(data->vendor);
    free(data->version);
    free(data->description);
}

void rlyeh_image_info_get_from_annotations(const char* image_location, const char* storage_location, const char* uri, rlyeh_image_info_t* data) {
    parser_error err = NULL;
    bool ret = false;
    char* short_imagename = NULL;
    image_spec_schema_image_index_schema* index_schema = NULL;
    image_spec_schema_image_manifest_schema* manifest_schema = NULL;
    amxc_string_t index_path;
    amxc_string_t manifest_name;
    rlyeh_image_parameters_t uri_elements;
    size_t it_annotations;
    amxc_string_init(&manifest_name, 0);
    amxc_string_init(&index_path, 64);
    rlyeh_image_parameters_init(&uri_elements);

    rlyeh_parse_uri(uri, &uri_elements);
    short_imagename = shorten_image_name(uri_elements.image_name);
    amxc_string_setf(&index_path, "%s/%s/index.json", image_location, uri_elements.image_name);
    data->disklocation = strdup(uri_elements.image_name);

    index_schema = rlyeh_parse_image_spec_schema(index_path.buffer);
    if(!index_schema) {
        SAH_TRACEZ_ERROR(ME, "Index schema not found [%s]", index_path.buffer);
        goto exit;
    }
    ret = build_manifest_filename_from_index(index_schema, uri_elements.version, storage_location, &manifest_name);
    when_failed(ret, exit);

    manifest_schema = image_spec_schema_image_manifest_schema_parse_file(manifest_name.buffer, 0, &err);

    if(manifest_schema && manifest_schema->annotations) {
        for(it_annotations = 0; it_annotations < manifest_schema->annotations->len; it_annotations++) {
            char* annotation_key = manifest_schema->annotations->keys[it_annotations];
            if(strcmp(RLYEH_ANNOTATION_OCI_DESCRIPTION, annotation_key) == 0) {
                data->description = strdup(manifest_schema->annotations->values[it_annotations]);
            } else if(strcmp(RLYEH_ANNOTATION_OCI_VENDOR, annotation_key) == 0) {
                data->vendor = strdup(manifest_schema->annotations->values[it_annotations]);
            } else if(strcmp(RLYEH_ANNOTATION_OCI_VERSION, annotation_key) == 0) {
                data->version = strdup(manifest_schema->annotations->values[it_annotations]);
            } else if(strcmp(RLYEH_ANNOTATION_OCI_REFNAME, annotation_key) == 0) {
                data->name = strdup(manifest_schema->annotations->values[it_annotations]);
            }
        }
    }

exit:
    fill_empty_field(short_imagename, uri_elements.version, data);
    free(err);
    free_image_spec_schema_image_manifest_schema(manifest_schema);
    free_image_spec_schema_image_index_schema(index_schema);
    amxc_string_clean(&manifest_name);
    amxc_string_clean(&index_path);
    rlyeh_image_parameters_clean(&uri_elements);
    free(short_imagename);
}